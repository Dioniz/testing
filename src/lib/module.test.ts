import { APP_BASE_HREF } from '@angular/common';
import { Type } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { TestBedConfig } from '@ngxa/utils';

import { AngularTestAbstract } from './abstracts/angular-test.abstract';

/**
 * Class helper to test Angular modules.
 */
export class ModuleTest<T extends object> extends AngularTestAbstract<T> {
  /**
   * Creates a class helper to test Angular modules.
   * @param clazz The class type to test.
   * @param [config] The custom module configuration needed to test the class.
   */
  public constructor(clazz: Type<T>, config?: TestBedConfig) {
    super(clazz, config);
    this.obj = this.testBed.get(clazz);
  }

  /**
   * Set the testBed property.
   * @param clazz The class type to test.
   * @param config The custom module configuration needed to test the class.
   * @returns The configured TestBed object.
   */
  protected setTestBed(clazz: Type<T>, config: TestBedConfig): typeof TestBed {
    config.imports.push(clazz);
    config.providers.push({ provide: APP_BASE_HREF, useValue: '/' });

    return super.setTestBed(clazz, config);
  }
}
