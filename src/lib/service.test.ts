import { Type } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { TestBedConfig } from '@ngxa/utils';

import { AngularTestAbstract } from './abstracts/angular-test.abstract';

/**
 * Class helper to test Angular services.
 */
export class ServiceTest<T extends object> extends AngularTestAbstract<T> {
  /**
   * Creates a class helper to test Angular services.
   * @param clazz The class type to test.
   * @param [config] The custom module configuration needed to test the class.
   */
  public constructor(clazz: Type<T>, config?: TestBedConfig) {
    super(clazz, config);
    this.obj = this.testBed.get(clazz);
  }

  /**
   * Set the testBed property.
   * @param clazz The class type to test.
   * @param config The custom module configuration needed to test the class.
   * @returns The configured TestBed object.
   */
  protected setTestBed(clazz: Type<T>, config: TestBedConfig): typeof TestBed {
    config.providers.push(clazz);

    return super.setTestBed(clazz, config);
  }
}
