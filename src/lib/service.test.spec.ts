import { Injectable } from '@angular/core';

import { ServiceTest } from './service.test';

@Injectable()
class TestService {}

describe('ServiceTest', () => {
  let test: ServiceTest<TestService>;

  beforeEach(() => {
    test = new ServiceTest(TestService);
  });

  it('.testBed have the service as provider', () => {
    test.isProvided(TestService);
  });

  it('.obj is setted with the service', () => {
    expect(test.obj).toEqual(new TestService());
  });
});
