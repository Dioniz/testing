import { HttpErrorResponse, HttpRequest, HttpResponse } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
  TestRequest
} from '@angular/common/http/testing';
import { Type } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { TestBedConfig, typeToString } from '@ngxa/utils';

import { ServiceTest } from './service.test';
import {
  TestErrorResponse,
  TestErrorResponseError,
  TestErrorResponseErrorInstance,
  TestErrorResponseErrorType,
  TestErrorResponseHeader,
  TestErrorResponseHeaderValue,
  TestErrorResponseStatus,
  TestErrorResponseStatusText,
  TestHttpCall,
  TestRequestBody,
  TestRequestBodyInstance,
  TestRequestBodyType,
  TestRequestHeader,
  TestRequestHeaderValue,
  TestRequestMethod,
  TestRequestParam,
  TestRequestParamValue,
  TestRequestResponseType,
  TestResponse,
  TestResponseBody,
  TestResponseBodyInstance,
  TestResponseBodyType,
  TestResponseHeader,
  TestResponseHeaderValue,
  TestResponseStatus,
  TestResponseStatusText,
  TestResponseValue,
  TestResponseValueInstance,
  TestResponseValueType
} from './typings/test-http.types';
import { testResponseOpts } from './typings/test-response-opts';
import { expectHasHeader } from './utils/expect-has-header';
import { expectHasNotHeader } from './utils/expect-has-not-header';
import { expectIsHttpResponse } from './utils/expect-is-http-response';

/**
 * Class helper to test Angular HTTP services.
 */
export class HttpClientServiceTest<T extends object> extends ServiceTest<T> {
  /**
   * Controller that allows for mocking and flushing of requests.
   */
  public httpMock: HttpTestingController;

  /**
   * Creates a class helper to test Angular HTTP services.
   * @param clazz The class type to test.
   * @param [config] The custom module configuration needed to test the class.
   */
  public constructor(clazz: Type<T>, config?: TestBedConfig) {
    super(clazz, config);
    this.httpMock = this.testBed.get(HttpTestingController);
  }

  /**
   * Set the testBed property.
   * @param clazz The class type to test.
   * @param config The custom module configuration needed to test the class.
   * @returns The configured TestBed object.
   */
  protected setTestBed(clazz: Type<T>, config: TestBedConfig): typeof TestBed {
    config.imports.push(HttpClientTestingModule);

    return super.setTestBed(clazz, config);
  }

  /**
   * Check if the method sends an HTTP request.
   * @param call The test HTTP request object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   * @returns The Http request.
   */
  public sendsRequest<V, O extends object = T>(
    call: TestHttpCall,
    obj: T | O = this.obj
  ): HttpRequest<V> {
    this.getObservableMethodValue(call, obj).subscribe();
    const req: HttpRequest<V> = this.httpMock.expectOne(
      (_req: HttpRequest<V>) => _req.url === call.path
    ).request;

    this.httpMock.verify();

    return req;
  }

  /**
   * Check if the method sends an HTTP request with an HTTP method.
   * @param call The test HTTP request method object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsRequestMethod<O extends object = T>(
    call: TestRequestMethod,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).method).toEqual(call.method);
  }

  /**
   * Check if the method do not sends an HTTP request with an HTTP method.
   * @param call The test HTTP request method object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestMethod<O extends object = T>(
    call: TestRequestMethod,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).method).not.toEqual(call.method);
  }

  /**
   * Check if the method sends an HTTP request with a body value.
   * @param call The test HTTP request body object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsRequestBody<V, O extends object = T>(
    call: TestRequestBody<V>,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).body).toEqual(call.body);
  }

  /**
   * Check if the method do not sends an HTTP request with a body value.
   * @param call The test HTTP request body object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestBody<V, O extends object = T>(
    call: TestRequestBody<V>,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).body).not.toEqual(call.body);
  }

  /**
   * Check if the method sends an HTTP request with a body type.
   * @param call The test HTTP request body type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsRequestBodyType<O extends object = T>(
    call: TestRequestBodyType,
    obj: T | O = this.obj
  ): void {
    expect(typeof this.sendsRequest(call, obj).body).toEqual(call.typeOf);
  }

  /**
   * Check if the method do not sends an HTTP request with a body type.
   * @param call The test HTTP request body type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestBodyType<O extends object = T>(
    call: TestRequestBodyType,
    obj: T | O = this.obj
  ): void {
    expect(typeof this.sendsRequest(call, obj).body).not.toEqual(call.typeOf);
  }

  /**
   * Check if the method sends an HTTP request with a body instance type.
   * @param call The test HTTP request body instance type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsRequestBodyInstance<V, O extends object = T>(
    call: TestRequestBodyInstance,
    obj: T | O = this.obj
  ): void {
    const req: HttpRequest<V> = this.sendsRequest(call, obj);

    expect(req.body instanceof call.instanceOf).toBeTruthy(
      `Expected request body instance type to be "${typeToString(
        call.instanceOf
      )}", but is "${typeToString(req.body)}"`
    );
  }

  /**
   * Check if the method do not sends an HTTP request with a body instance type.
   * @param call The test HTTP request body instance type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestBodyInstance<O extends object = T>(
    call: TestRequestBodyInstance,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).body instanceof call.instanceOf).toBeFalsy(
      `Expected request body instance type not to be "${typeToString(call.instanceOf)}", but it is`
    );
  }

  /**
   * Check if the method sends an HTTP request with a header.
   * @param call The test HTTP request header object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   * @returns The Http request object.
   */
  public sendsRequestHeader<V, O extends object = T>(
    call: TestRequestHeader,
    obj: T | O = this.obj
  ): HttpRequest<V> {
    const req: HttpRequest<V> = this.sendsRequest(call, obj);

    expectHasHeader(req, call.header);

    return req;
  }

  /**
   * Check if the method do not sends an HTTP request with a header.
   * @param call The test HTTP request header object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestHeader<O extends object = T>(
    call: TestRequestHeader,
    obj: T | O = this.obj
  ): void {
    expectHasNotHeader(this.sendsRequest(call, obj), call.header);
  }

  /**
   * Check if the method sends an HTTP request with a header value.
   * @param call The test HTTP request header value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsRequestHeaderValue<O extends object = T>(
    call: TestRequestHeaderValue,
    obj: T | O = this.obj
  ): void {
    expect(expectHasHeader(this.sendsRequestHeader(call, obj), call.header)).toMatch(call.value);
  }

  /**
   * Check if the method do not sends an HTTP request with a header value.
   * @param call The test HTTP request header value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestHeaderValue<O extends object = T>(
    call: TestRequestHeaderValue,
    obj: T | O = this.obj
  ): void {
    expect(expectHasHeader(this.sendsRequestHeader(call, obj), call.header)).not.toMatch(
      call.value
    );
  }

  /**
   * Check if the method sends an HTTP request with a param.
   * @param call The test HTTP request param object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   * @returns The Http request object.
   */
  public sendsRequestParam<V, O extends object = T>(
    call: TestRequestParam,
    obj: T | O = this.obj
  ): HttpRequest<V> {
    const req: HttpRequest<V> = this.sendsRequest(call, obj);

    expect(req.params.has(call.param)).toBeTruthy(
      `Expected params to contain "${call.param}", but are "${req.params.keys().toString()}"`
    );

    return req;
  }

  /**
   * Check if the method no not sends an HTTP request with a param.
   * @param call The test HTTP request param object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestParam<O extends object = T>(
    call: TestRequestParam,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).params.has(call.param)).toBeFalsy(
      `Expected params do not to contain "${call.param}", but it has`
    );
  }

  /**
   * Check if the method sends an HTTP request with a param value.
   * @param call The test HTTP request param value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsRequestParamValue<O extends object = T>(
    call: TestRequestParamValue,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequestParam(call, obj).params.get(call.param)).toMatch(call.value);
  }

  /**
   * Check if the method do not sends an HTTP request with a param value.
   * @param call The test HTTP request param value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestParamValue<O extends object = T>(
    call: TestRequestParamValue,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequestParam(call, obj).params.get(call.param)).not.toMatch(call.value);
  }

  /**
   * Check if the method sends an HTTP request with credentials.
   * @param call The test HTTP request object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsRequestWithCredentials<O extends object = T>(
    call: TestHttpCall,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).withCredentials).toBeTruthy(
      'Expected withCredentials to be "true", but is "false"'
    );
  }

  /**
   * Check if the method do not sends an HTTP request with credentials.
   * @param call The test HTTP request object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestWithCredentials<O extends object = T>(
    call: TestHttpCall,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).withCredentials).toBeFalsy(
      'Expected withCredentials to be "false", but is "true"'
    );
  }

  /**
   * Check if the method sends an HTTP request with report progress.
   * @param call The test HTTP request object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsRequestReportProgress<O extends object = T>(
    call: TestHttpCall,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).reportProgress).toBeTruthy(
      'Expected reportProgress to be "true", but is "false"'
    );
  }

  /**
   * Check if the method do not sends an HTTP request with report progress.
   * @param call The test HTTP request object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestReportProgress<O extends object = T>(
    call: TestHttpCall,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).reportProgress).toBeFalsy(
      'Expected reportProgress to be "false", but is "true"'
    );
  }

  /**
   * Check if the method sends an HTTP request with a response type.
   * @param call The test HTTP request response type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsRequestResponseType<O extends object = T>(
    call: TestRequestResponseType,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).responseType).toEqual(call.responseType);
  }

  /**
   * Check if the method do not sends an HTTP request with a response type.
   * @param call The test HTTP request response type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public sendsNotRequestResponseType<O extends object = T>(
    call: TestRequestResponseType,
    obj: T | O = this.obj
  ): void {
    expect(this.sendsRequest(call, obj).responseType).not.toEqual(call.responseType);
  }

  /**
   * Check if the method returns an HTTP response.
   * @param call The test HTTP response object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponse<V, O extends object = T>(
    call: TestResponse<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: V | {} | null) => expect(next).toBeDefined());
  }

  /**
   * Check if the method returns an HTTP response value.
   * @param call The test HTTP response value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponseValue<V, K = V, O extends object = T>(
    call: TestResponseValue<V, K>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: V | {} | null) => expect(next).toEqual(call.value));
  }

  /**
   * Check if the method do not returns an HTTP response value.
   * @param call The test HTTP response value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotResponseValue<V, K = V, O extends object = T>(
    call: TestResponseValue<V, K>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: V | {} | null) => expect(next).not.toEqual(call.value));
  }

  /**
   * Check if the method returns an HTTP response value type.
   * @param call The test HTTP response type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponseValueType<V, O extends object = T>(
    call: TestResponseValueType<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: V | {} | null) => {
      expect(typeof next).toEqual(call.typeOf);
    });
  }

  /**
   * Check if the method do not returns an HTTP response value type.
   * @param call The test HTTP response type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotResponseValueType<V, O extends object = T>(
    call: TestResponseValueType<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: V | {} | null) =>
      expect(typeof next).not.toEqual(call.typeOf)
    );
  }

  /**
   * Check if the method returns an HTTP response value instance type.
   * @param call The test HTTP response instance type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponseValueInstance<V, O extends object = T>(
    call: TestResponseValueInstance<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: {} | null) => {
      expect(next instanceof call.instanceOf).toBeTruthy(
        `Expected response instance type to be "${typeToString(
          call.instanceOf
        )}", but is "${typeToString(next)}"`
      );
    });
  }

  /**
   * Check if the method do not returns an HTTP response value instance type.
   * @param call The test HTTP response instance type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotResponseValueInstance<V, O extends object = T>(
    call: TestResponseValueInstance<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: {} | null) =>
      expect(next instanceof call.instanceOf).toBeFalsy(
        `Expected response instance type not to be "${typeToString(call.instanceOf)}", but it is`
      )
    );
  }

  /**
   * Check if the method returns an HTTP response body.
   * @param call The test HTTP response body object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponseBody<V, O extends object = T>(
    call: TestResponseBody<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: V | {} | null) =>
      expect(expectIsHttpResponse(next).body).toEqual(call.body)
    );
  }

  /**
   * Check if the method do not returns an HTTP response body.
   * @param call The test HTTP response body object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotResponseBody<V, O extends object = T>(
    call: TestResponseBody<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: V | {} | null) =>
      expect(expectIsHttpResponse(next).body).not.toEqual(call.body)
    );
  }

  /**
   * Check if the method returns an HTTP response type.
   * @param call The test HTTP response type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponseBodyType<V, O extends object = T>(
    call: TestResponseBodyType<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: V | {} | null) => {
      expect(typeof expectIsHttpResponse(next).body).toEqual(call.typeOf);
    });
  }

  /**
   * Check if the method do not returns an HTTP response type.
   * @param call The test HTTP response type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotResponseBodyType<V, O extends object = T>(
    call: TestResponseBodyType<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: V | {} | null) =>
      expect(typeof expectIsHttpResponse(next).body).not.toEqual(call.typeOf)
    );
  }

  /**
   * Check if the method returns an HTTP response instance type.
   * @param call The test HTTP response instance type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponseBodyInstance<V, O extends object = T>(
    call: TestResponseBodyInstance<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: {} | null) => {
      expect(expectIsHttpResponse(next).body instanceof call.instanceOf).toBeTruthy(
        `Expected response body instance type to be "${typeToString(
          call.instanceOf
        )}", but is "${typeToString(next)}"`
      );
    });
  }

  /**
   * Check if the method do not returns an HTTP response instance type.
   * @param call The test HTTP response instance type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotResponseBodyInstance<V, O extends object = T>(
    call: TestResponseBodyInstance<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: {} | null) =>
      expect(expectIsHttpResponse(next).body instanceof call.instanceOf).toBeFalsy(
        `Expected response body instance type not to be "${typeToString(
          call.instanceOf
        )}", but it is`
      )
    );
  }

  /**
   * Check if the method returns an HTTP response status.
   * @param call The test HTTP response status object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponseStatus<V, O extends object = T>(
    call: TestResponseStatus<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: HttpResponse<V> | {} | null) => {
      expect(expectIsHttpResponse(next).status).toEqual(call.status);
    });
  }

  /**
   * Check if the method do not returns an HTTP response status.
   * @param call The test HTTP response status object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotResponseStatus<V, O extends object = T>(
    call: TestResponseStatus<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: HttpResponse<V> | {} | null) => {
      expect(expectIsHttpResponse(next).status).not.toEqual(call.status);
    });
  }

  /**
   * Check if the method returns an HTTP response statusText.
   * @param call The test HTTP response statusText object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponseStatusText<V, O extends object = T>(
    call: TestResponseStatusText<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: HttpResponse<V> | {} | null) => {
      expect(expectIsHttpResponse(next).statusText).toEqual(call.statusText);
    });
  }

  /**
   * Check if the method do not returns an HTTP response statusText.
   * @param call The test HTTP response statusText object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotResponseStatusText<V, O extends object = T>(
    call: TestResponseStatusText<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: HttpResponse<V> | {} | null) => {
      expect(expectIsHttpResponse(next).statusText).not.toEqual(call.statusText);
    });
  }

  /**
   * Check if the method returns an HTTP response header.
   * @param call The test HTTP response header object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponseHeader<V, O extends object = T>(
    call: TestResponseHeader<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: HttpResponse<V> | {} | null) => {
      expectHasHeader(expectIsHttpResponse(next), call.header);
    });
  }

  /**
   * Check if the method do not returns an HTTP response header.
   * @param call The test HTTP response header object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotResponseHeader<V, O extends object = T>(
    call: TestResponseHeader<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: HttpResponse<V> | {} | null) => {
      expectHasNotHeader(expectIsHttpResponse(next), call.header);
    });
  }

  /**
   * Check if the method returns an HTTP response header value.
   * @param call The test HTTP response header value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsResponseHeaderValue<V, O extends object = T>(
    call: TestResponseHeaderValue<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: HttpResponse<V> | {} | null) => {
      expect(expectHasHeader(expectIsHttpResponse(next), call.header)).toMatch(call.value);
    });
  }

  /**
   * Check if the method do not returns an HTTP response header value.
   * @param call The test HTTP response header value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotResponseHeaderValue<V, O extends object = T>(
    call: TestResponseHeaderValue<V>,
    obj: T | O = this.obj
  ): void {
    this._testResponse(call, obj, (next: HttpResponse<V> | {} | null) => {
      expect(expectHasHeader(expectIsHttpResponse(next), call.header)).not.toMatch(call.value);
    });
  }

  private _testResponse<V, O extends object>(
    call: TestResponse<V>,
    obj: O,
    fn: (error: V | {} | null) => void
  ): void {
    this.getObservableMethodValue(call, obj).subscribe(fn);

    const req: TestRequest = this.httpMock.expectOne(
      (_req: HttpRequest<V>) => _req.url === call.path
    );

    req.flush(call.response.body, testResponseOpts(call));
    this.httpMock.verify();
  }

  /**
   * Check if the method returns an HTTP error response.
   * @param call The test HTTP error response object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsErrorResponse<O extends object = T>(
    call: TestErrorResponse,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(error).toBeDefined();
    });
  }

  /**
   * Check if the method returns an HTTP error response value.
   * @param call The test HTTP error response value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsErrorResponseError<V, O extends object = T>(
    call: TestErrorResponseError<V>,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(error.error).toEqual(call.error);
    });
  }

  /**
   * Check if the method do not returns an HTTP error response value.
   * @param call The test HTTP error response value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotErrorResponseError<V, O extends object = T>(
    call: TestErrorResponseError<V>,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(error.error).not.toEqual(call.error);
    });
  }

  /**
   * Check if the method returns an HTTP error response type.
   * @param call The test HTTP error response type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsErrorResponseErrorType<O extends object = T>(
    call: TestErrorResponseErrorType,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(typeof error.error).toEqual(call.typeOf);
    });
  }

  /**
   * Check if the method do not returns an HTTP error response type.
   * @param call The test HTTP error response type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotErrorResponseErrorType<O extends object = T>(
    call: TestErrorResponseErrorType,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(typeof error.error).not.toEqual(call.typeOf);
    });
  }

  /**
   * Check if the method returns an HTTP error response instance type.
   * @param call The test HTTP error response instance type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsErrorResponseErrorInstance<O extends object = T>(
    call: TestErrorResponseErrorInstance,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(error.error instanceof call.instanceOf).toBeTruthy(
        `Expected error instance type to be "${typeToString(
          call.instanceOf
        )}", but is "${typeToString(error)}"`
      );
    });
  }

  /**
   * Check if the method do not returns an HTTP error response instance type.
   * @param call The test HTTP error response instance type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotErrorResponseErrorInstance<O extends object = T>(
    call: TestErrorResponseErrorInstance,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(error.error instanceof call.instanceOf).toBeFalsy(
        `Expected error instance type not to be "${typeToString(call.instanceOf)}", but it is`
      );
    });
  }

  /**
   * Check if the method returns an HTTP error response status.
   * @param call The test HTTP error response status object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsErrorResponseStatus<O extends object = T>(
    call: TestErrorResponseStatus,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(error.status).toEqual(call.status);
    });
  }

  /**
   * Check if the method do not returns an HTTP error response status.
   * @param call The test HTTP error response status object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotErrorResponseStatus<O extends object = T>(
    call: TestErrorResponseStatus,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(error.status).not.toEqual(call.status);
    });
  }

  /**
   * Check if the method returns an HTTP error response statusText.
   * @param call The test HTTP error response statusText object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsErrorResponseStatusText<O extends object = T>(
    call: TestErrorResponseStatusText,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(error.statusText).toEqual(call.statusText);
    });
  }

  /**
   * Check if the method do not returns an HTTP error response statusText.
   * @param call The test HTTP error response statusText object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotErrorResponseStatusText<O extends object = T>(
    call: TestErrorResponseStatusText,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(error.statusText).not.toEqual(call.statusText);
    });
  }

  /**
   * Check if the method returns an HTTP error response header.
   * @param call The test HTTP error response header object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsErrorResponseHeader<O extends object = T>(
    call: TestErrorResponseHeader,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expectHasHeader(error, call.header);
    });
  }

  /**
   * Check if the method do not returns an HTTP error response header.
   * @param call The test HTTP error response header object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotErrorResponseHeader<O extends object = T>(
    call: TestErrorResponseHeader,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expectHasNotHeader(error, call.header);
    });
  }

  /**
   * Check if the method returns an HTTP error response header value.
   * @param call The test HTTP error response header value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsErrorResponseHeaderValue<O extends object = T>(
    call: TestErrorResponseHeaderValue,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(expectHasHeader(error, call.header)).toMatch(call.value);
    });
  }

  /**
   * Check if the method do not returns an HTTP error response header value.
   * @param call The test HTTP error response header value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  public returnsNotErrorResponseHeaderValue<O extends object = T>(
    call: TestErrorResponseHeaderValue,
    obj: T | O = this.obj
  ): void {
    this._testErrorResponse(call, obj, (error: HttpErrorResponse) => {
      expect(expectHasHeader(error, call.header)).not.toMatch(call.value);
    });
  }

  private _testErrorResponse<V, O extends object>(
    call: TestErrorResponse,
    obj: O,
    fn: (error: HttpErrorResponse) => void
  ): void {
    this.getObservableMethodValue(call, obj).subscribe(undefined, fn);

    const req: TestRequest = this.httpMock.expectOne(
      (_req: HttpRequest<V>) => _req.url === call.path
    );

    if (call.response.error instanceof ErrorEvent) {
      req.error(call.response.error, testResponseOpts(call));
    } else {
      req.flush(call.response.error, testResponseOpts(call));
    }
    this.httpMock.verify();
  }
}
