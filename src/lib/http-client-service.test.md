# How to test an HttpClient service

First you must define the `HttpClientServiceTest` object. If the service have dependencies, we must create the `TestBedConfig` object and pass it as param to the HttpClientServiceTest. Otherwise, it can be omitted. `HttpClientModule` can be omitted because is preinjected.

It also defines `HttpTestingController` to mock an HTTP call.

```typescript
describe('TestHttpCLientService', () => {
  let test: HttpClientServiceTest<TestHttpCLientService>;

  beforeEach(() => {
    test = new HttpClientServiceTest(TestHttpCLientService);
  });
});
```

Then we can use all [common class helpers](https://ngxa.gitlab.io/testing/classes/ClassTest.html), as well all [specific Angular helpers](https://ngxa.gitlab.io/testing/classes/AngularTestAbstract.html). The service object is under `test.obj`. In addition, we can use the next helpers.

## Request helpers

To test an HTTP request we can use the helpers to test every aspect. An optional `args` property can be set to make the object key method call.

```ts
test.sendsRequest({ key: 'method', path: '/');
test.sendsRequestMethod({ key: 'method', method: 'GET', path: '/' });
test.sendsRequestBody({ key: 'method', body: 'a', path: '/' });
test.sendsRequestBodyType({ key: 'method', typeOf: 'object', path: '/' });
test.sendsRequestBodyInstance({ key: 'method', instanceOf: AClass, path: '/' });
test.sendsRequestHeader({ key: 'method', header: 'h1', path: '/' });
test.sendsRequestHeaderValue({ key: 'method', header: 'h1', value: 'a', path: '/' });
test.sendsRequestParam({ key: 'method', param: 'p1', path: '/' });
test.sendsRequestParamValue({ key: 'method', param: 'p1', value: 'a', path: '/' });
test.sendsRequestWithCredentials({ key: 'method', path: '/' });
test.sendsRequestReportProgress({ key: 'method', path: '/' });
test.sendsRequestResponseType({ key: 'method', responseType: 'json', path: '/' });
```

## Response helpers

To test an HTTP response we have to mock one with an `HttpResponse` object.

```ts
const response: HttpResponse<any> = new HttpResponse({
  body: { a: a },
  status: 200,
  statusText: 'OK',
  headers: new HttpHeaders({ h1: 'a' });
});
```

Then we can test every response element.

**IMPORTANT**: Note that to test a response HTTP element other that returned method value you **MUST** add `{ observe: 'response' }` to the call.

Due to map or other transformation methods the `value` and `body` can differ. Body will be the data returned by the HTTP call. Value will be the `Observable` value returned.

```ts
this.http.get(this.path, { observe: 'response' });
```

```ts
test.returnsResponse({ key: 'method', path: '/', response });
this.returnsResponseValue({ key: 'method', value: 'a', path: '/', response });
test.returnsResponseValueType({ key: 'method', typeOf: 'object', path: '/', response });
test.returnsResponseValueInstance({ key: 'method', instanceOf: AClass, path: '/', response });
test.returnsResponseBody({ key: 'method', body: 'a', path: '/', response });
test.returnsResponseBodyType({ key: 'method', typeOf: 'object', path: '/', response });
test.returnsResponseBodyInstance({ key: 'method', instanceOf: AClass, path: '/', response });
test.returnsResponseStatus({ key: 'method', status: 200, path: '/', response });
test.returnsResponseStatusText({ key: 'method', statusText: 'OK', path: '/', response });
test.returnsResponseHeader({ key: 'method', header: 'h1', path: '/', response });
test.returnsResponseHeaderValue({ key: 'method', header: 'h1', value: 'a', path: '/', response });
```

## Response error helpers

To test an HTTP error response we have to mock one with an `HttpErrorResponse` object.

```ts
const response: HttpErrorResponse = new HttpErrorResponse({
  error: { a: a }, // or new ErrorEvent('error')
  status: 400,
  statusText: 'BAD REQUEST',
  headers: new HttpHeaders({ h1: 'a' });
});
```

Then we can test every response element.

```ts
test.returnsErrorResponse({ key: 'method', path: '/', response });
test.returnsErrorResponseError({ key: 'method', body: 'a', path: '/', response });
test.returnsErrorResponseErrorType({ key: 'method', typeOf: 'object', path: '/', response });
test.returnsErrorResponseErrorInstance({ key: 'method', instanceOf: AClass, path: '/', response });
test.returnsErrorResponseStatus({ key: 'method', status: 400, path: '/', response });
test.returnsErrorResponseStatusText({
  key: 'method',
  statusText: 'BAD REQUEST',
  path: '/',
  response
});
test.returnsErrorResponseHeader({ key: 'method', header: 'h1', path: '/', response });
test.returnsErrorResponseHeaderValue({
  key: 'method',
  header: 'h1',
  value: 'a',
  path: '/',
  response
});
```
