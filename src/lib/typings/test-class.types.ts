import { TypeofLiterals } from '@ngxa/utils';

/**
 * The args needed to test a member call.
 */
export interface TestCall {
  /**
   * The member key to check.
   */
  key: PropertyKey;
  /**
   * The args needed to call the method key.
   */
  args?: any[];
}

/**
 * The args needed to test a value.
 */
export interface TestValue<T> {
  /**
   * The expected value.
   */
  value: T;
}

/**
 * The args needed to test an error.
 */
export interface TestError<T> {
  /**
   * The expected error.
   */
  error: T;
}

/**
 * The args needed to test a member value typeof.
 */
export interface TestType {
  /**
   * The expected value type.
   */
  typeOf: TypeofLiterals;
}

/**
 * The args needed to test a member value object instanceof.
 */
export interface TestInstance {
  /**
   * The expected value instance.
   */
  instanceOf: any;
}

/**
 * The args needed to test a member value.
 */
export type TestHasValue<T> = TestCall & TestValue<T>;

/**
 * The args needed to test a member value type.
 */
export type TestHasValueType = TestCall & TestType;

/**
 * The args needed to test a member value instance type.
 */
export type TestHasValueInstance = TestCall & TestInstance;

/**
 * The args needed to test a member error.
 */
export type TestHasError<T> = TestCall & TestError<T>;

/**
 * The args needed to test a member error type.
 */
export type TestHasErrorType = TestCall & TestType;

/**
 * The args needed to test a member error instance type.
 */
export type TestHasErrorInstance = TestCall & TestInstance;

/**
 * The args needed to test a method have been called from an object.
 */
export interface TestFromCall {
  /**
   * The method key to call before the member check.
   */
  fromKey: PropertyKey;
  /**
   * The args needed to call the method key to call before the member check.
   */
  fromArgs?: any[];
}

/**
 * The object to be spied.
 */
export interface TestCalledObject<O extends object> {
  /**
   * The object to spy.
   */
  spiedObj?: O;
}

/**
 * The args needed to test a method from an object have been called.
 */
export type TestCalled<O extends object> = TestFromCall & TestCalledObject<O> & TestCall;

/**
 * The args needed to test a console ouput.
 */
export interface TestConsole extends TestFromCall {
  /**
   * The console output level.
   */
  level: keyof Console;
  /**
   * The console call args.
   */
  args: any[];
}
