import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { HttpMethodLiterals, HttpResponseTypeLiterals } from '@ngxa/utils';

import { TestCall, TestError, TestInstance, TestType, TestValue } from './test-class.types';

/**
 * The test HTTP call.
 */
export interface TestHttpCall extends TestCall {
  /**
   * The HTTP path to check.
   */
  path: string;
}

/**
 * The test HTTP body.
 */
export interface TestHttpBody<T> {
  /**
   * The HTTP body to check.
   */
  body: T;
}

/**
 * The test HTTP status.
 */
export interface TestHttpStatus {
  /**
   * The HTTP status to check.
   */
  status: number;
}

/**
 * The test HTTP statusText.
 */
export interface TestHttpStatusText {
  /**
   * The HTTP statusText to check.
   */
  statusText: string;
}

/**
 * The test HTTP header.
 */
export interface TestHttpHeader {
  /**
   * TThe HTTP header to check.
   */
  header: string;
}

/**
 * The test HTTP request method.
 */
export interface TestRequestMethod extends TestHttpCall {
  /**
   * The HTTP request method to check.
   */
  method: HttpMethodLiterals;
}

/**
 * The test HTTP request body.
 */
export type TestRequestBody<T> = TestHttpCall & TestHttpBody<T>;

/**
 * The test HTTP request body type.
 */
export type TestRequestBodyType = TestHttpCall & TestType;

/**
 * The test HTTP request body instance type.
 */
export type TestRequestBodyInstance = TestHttpCall & TestInstance;

/**
 * The test HTTP request status.
 */
export type TestRequestStatus = TestHttpCall & TestHttpStatus;

/**
 * The test HTTP request statusText.
 */
export type TestRequestStatusText = TestHttpCall & TestHttpStatusText;

/**
 * The test HTTP request header.
 */
export type TestRequestHeader = TestHttpCall & TestHttpHeader;

/**
 * The test HTTP request header value.
 */
export type TestRequestHeaderValue = TestRequestHeader & TestValue<string | RegExp>;

/**
 * The test HTTP request param.
 */
export interface TestRequestParam extends TestHttpCall {
  /**
   * The HTTP request param.
   */
  param: string;
}

/**
 * The test HTTP request param value.
 */
export type TestRequestParamValue = TestRequestParam & TestValue<string | RegExp>;

/**
 * The test HTTP request response type.
 */
export interface TestRequestResponseType extends TestHttpCall {
  /**
   * The HTTP request response type.
   */
  responseType: HttpResponseTypeLiterals;
}

/**
 * The test HTTP response.
 */
export interface TestResponse<T> extends TestHttpCall {
  /**
   * The HTTP response.
   */
  response: HttpResponse<T>;
}

/**
 * The test HTTP response value.
 */
export type TestResponseValue<T, K = T> = TestResponse<T> & TestValue<K>;

/**
 * The test HTTP response value type.
 */
export type TestResponseValueType<T> = TestResponse<T> & TestType;

/**
 * The test HTTP response value instance type.
 */
export type TestResponseValueInstance<T> = TestResponse<T> & TestInstance;

/**
 * The test HTTP response body.
 */
export type TestResponseBody<T> = TestResponse<T> & TestHttpBody<T>;

/**
 * The test HTTP response body type.
 */
export type TestResponseBodyType<T> = TestResponse<T> & TestType;

/**
 * The test HTTP response body instance type.
 */
export type TestResponseBodyInstance<T> = TestResponse<T> & TestInstance;

/**
 * The test HTTP response status.
 */
export type TestResponseStatus<T> = TestResponse<T> & TestHttpStatus;

/**
 * The test HTTP response statusText.
 */
export type TestResponseStatusText<T> = TestResponse<T> & TestHttpStatusText;

/**
 * The test HTTP response header.
 */
export type TestResponseHeader<T> = TestResponse<T> & TestHttpHeader;

/**
 * The test HTTP response header value.
 */
export type TestResponseHeaderValue<T> = TestResponseHeader<T> & TestValue<string | RegExp>;

/**
 * The test HTTP error response.
 */
export interface TestErrorResponse extends TestHttpCall {
  /**
   * The HTTP error response.
   */
  response: HttpErrorResponse;
}

/**
 * The test HTTP error response error.
 */
export type TestErrorResponseError<T> = TestErrorResponse & TestError<T>;

/**
 * The test HTTP error response error type.
 */
export type TestErrorResponseErrorType = TestErrorResponse & TestType;

/**
 * The test HTTP error response error instance type.
 */
export type TestErrorResponseErrorInstance = TestErrorResponse & TestInstance;

/**
 * The test HTTP error response status.
 */
export type TestErrorResponseStatus = TestErrorResponse & TestHttpStatus;

/**
 * The test HTTP error response statusText.
 */
export type TestErrorResponseStatusText = TestErrorResponse & TestHttpStatusText;

/**
 * The test HTTP error response header.
 */
export type TestErrorResponseHeader = TestErrorResponse & TestHttpHeader;

/**
 * The test HTTP error response header value.
 */
export type TestErrorResponseHeaderValue = TestErrorResponseHeader & TestValue<string | RegExp>;
