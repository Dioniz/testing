import { GrandTestClass, OtherTestClass, TestClass, testData, TestEntry } from '@ngxa/utils';
import { asyncScheduler, of, throwError } from 'rxjs';

import { ClassTestAbstract } from './class-test.abstract';

const otherObj: OtherTestClass = new OtherTestClass();

class LocalTestClass extends TestClass {
  public fireEvent(event: Event): void {
    event.stopPropagation();
  }

  public fireOther(): void {
    otherObj.otherMethod();
  }

  public consoleLog(value: string): void {
    console.log(value);
  }
}

class ClassTest extends ClassTestAbstract<LocalTestClass> {}

describe('ClassTestValuesAbstract', () => {
  let test: ClassTest;
  let other: OtherTestClass;

  beforeEach(() => {
    test = new ClassTest();
    test.obj = new LocalTestClass();
    other = new OtherTestClass();
  });

  it('.obj is set', () => {
    expect(test.obj).toBeDefined();
    expect(test.obj).toEqual(new LocalTestClass());
  });

  // #region .isInstantiable(obj?)

  it('.isInstantiable() test if is instantiable', () => {
    test.isInstantiable();
  });

  it('.isInstantiable(obj) test if object is instantiable', () => {
    test.isInstantiable(other);
  });

  // #endregion

  // #region .inheritsFrom(parent, obj?)

  it('.inheritsFrom(parent) test a class inherith directly', () => {
    test.inheritsFrom(TestClass);
  });

  it('.inheritsFrom(parent) test a class inherith indirectly', () => {
    test.inheritsFrom(GrandTestClass);
  });

  it('.inheritsFrom(parent, obj) test a class inherith with object', () => {
    test.inheritsFrom(GrandTestClass, new TestClass());
  });

  // #endregion

  // #region .notInheritsFrom(parent, obj?)

  it('.notInheritsFrom(parent) test a class do not inherith', () => {
    test.notInheritsFrom(OtherTestClass);
  });

  it('.notInheritsFrom(parent, obj) test a class do not inherith with object', () => {
    test.notInheritsFrom(TestClass, new GrandTestClass());
  });

  // #endregion

  // #region .isMember(key, obj?)

  it('.isMember(key) test property as member', () => {
    test.isMember('prop');
    test.isMember('grandProp');
  });

  it('.isMember(key) test getter/setter as member', () => {
    test.isMember('getterSetterProp');
    test.isMember('getterSetterGrandProp');
  });

  it('.isMember(key) test getter as member', () => {
    test.isMember('getterProp');
    test.isMember('getterGrandProp');
  });

  it('.isMember(key) test setter as member', () => {
    test.isMember('setterProp');
    test.isMember('setterGrandProp');
  });

  it('.isMember(key) test method as member', () => {
    test.isMember('method');
    test.isMember('grandMethod');
  });

  it('.isMember(key, obj) test property as member with object', () => {
    test.isMember('otherProp', other);
  });

  it('.isMember(key, obj) test getter/setter as member with object', () => {
    test.isMember('getterSetterOtherProp', other);
  });

  it('.isMember(key, obj) test getter as member with object', () => {
    test.isMember('getterOtherProp', other);
  });

  it('.isMember(key, obj) test setter as member with object', () => {
    test.isMember('setterOtherProp', other);
  });

  it('.isMember(key, obj) test method as member with object', () => {
    test.isMember('otherMethod', other);
  });

  // #endregion

  // #region .isNotMember(key, obj?)

  it('.isNotMember(key) test non existant as not a member', () => {
    test.isNotMember('invalidMember');
  });

  it('.isNotMember(key) test undefined property as not a member', () => {
    test.isNotMember('undefinedProp');
    test.isNotMember('undefinedGrandProp');
  });

  it('.isNotMember(key, obj) test non existant as not a member with object', () => {
    test.isNotMember('invalidMember', other);
  });

  it('.isNotMember(key, obj) test undefined property as not a member with object', () => {
    test.isNotMember('undefinedOtherProp', other);
  });

  // #endregion

  // #region .isProperty(key, obj?)

  it('.isNotProperty(key) test non existant as not a property', () => {
    test.isNotProperty('invalidProp');
  });

  it('.isProperty(key) test property as property', () => {
    test.isProperty('prop');
    test.isProperty('grandProp');
  });

  it('.isNotProperty(key) test undefined property as not a property', () => {
    test.isNotProperty('undefinedProp');
    test.isNotProperty('undefinedGrandProp');
  });

  it('.isProperty(key) test getter/setter as property', () => {
    test.isProperty('getterSetterProp');
    test.isProperty('getterSetterGrandProp');
  });

  it('.isProperty(key) test getter as property', () => {
    test.isProperty('getterProp');
    test.isProperty('getterGrandProp');
  });

  it('.isProperty(key) test setter as property', () => {
    test.isProperty('setterProp');
    test.isProperty('setterGrandProp');
  });

  it('.isNotProperty(key) test method as not a property', () => {
    test.isNotProperty('method');
    test.isNotProperty('grandMethod');
  });

  it('.isNotProperty(key, obj) test non existant as not a property with object', () => {
    test.isNotProperty('invalidProp', other);
  });

  it('.isProperty(key, obj) test property as property with object', () => {
    test.isProperty('otherProp', other);
  });

  it('.isNotProperty(key, obj) test undefined property as not a property with object', () => {
    test.isNotProperty('undefinedOtherProp', other);
  });

  it('.isProperty(key, obj) test getter/setter as property with object', () => {
    test.isProperty('getterSetterOtherProp', other);
  });

  it('.isProperty(key, obj) test getter as property with object', () => {
    test.isProperty('getterOtherProp', other);
  });

  it('.isProperty(key, obj) test setter as property with object', () => {
    test.isProperty('setterOtherProp', other);
  });

  it('.isNotProperty(key, obj) test method as not a property with object', () => {
    test.isNotProperty('otherMethod', other);
  });

  // #endregion

  // #region .isGetterSetter(key, obj?)

  it('.isNotGetterSetter(key) test non existant as not a getter/setter', () => {
    test.isNotGetterSetter('invalidGetterSetter');
  });

  it('.isNotGetterSetter(key) test property as not a getter/setter', () => {
    test.isNotGetterSetter('prop');
    test.isNotGetterSetter('grandProp');
  });

  it('.isGetterSetter(key) test getter/setter as getter/setter', () => {
    test.isGetterSetter('getterSetterProp');
    test.isGetterSetter('getterSetterGrandProp');
  });

  it('.isGetterSetter(key) test getter as getter/setter', () => {
    test.isGetterSetter('getterProp');
    test.isGetterSetter('getterGrandProp');
  });

  it('.isGetterSetter(key) test setter as getter/setter', () => {
    test.isGetterSetter('setterProp');
    test.isGetterSetter('setterGrandProp');
  });

  it('.isNotGetterSetter(key) test method as not a getter/setter', () => {
    test.isNotGetterSetter('method');
    test.isNotGetterSetter('grandMethod');
  });

  it('.isNotGetterSetter(key, obj) test non existant as not a getter/setter with object', () => {
    test.isNotGetterSetter('invalidGetterSetter', other);
  });

  it('.isNotGetterSetter(key, obj) test property as not a getter/setter with object', () => {
    test.isNotGetterSetter('otherProp', other);
  });

  it('.isGetterSetter(key, obj) test getter/setter as getter/setter with object', () => {
    test.isGetterSetter('getterSetterOtherProp', other);
  });

  it('.isGetterSetter(key, obj) test getter as getter/setter with object', () => {
    test.isGetterSetter('getterOtherProp', other);
  });

  it('.isGetterSetter(key, obj) test setter as getter/setter with object', () => {
    test.isGetterSetter('setterOtherProp', other);
  });

  it('.isNotGetterSetter(key, obj) test method as not a getter/setter with object', () => {
    test.isNotGetterSetter('otherMethod', other);
  });

  // #endregion

  // #region .isGettable(key, obj?)

  it('.isNotGettable(key) test non existant as not a gettable', () => {
    test.isNotGettable('invalidGetter');
  });

  it('.isGettable(key) test property as gettable', () => {
    test.isGettable('prop');
    test.isGettable('grandProp');
  });

  it('.isGettable(key) test getter/setter as gettable', () => {
    test.isGettable('getterSetterProp');
    test.isGettable('getterSetterGrandProp');
  });

  it('.isGettable(key) test getter as gettable', () => {
    test.isGettable('getterProp');
    test.isGettable('getterGrandProp');
  });

  it('.isNotGettable(key) test setter as not a gettable', () => {
    test.isNotGettable('setterProp');
    test.isNotGettable('setterGrandProp');
  });

  it('.isNotGettable(key) test method as not a gettable', () => {
    test.isNotGettable('method');
    test.isNotGettable('grandMethod');
  });

  it('.isNotGettable(key, obj) test non existant as not a gettable with object', () => {
    test.isNotGettable('invalidGetter', other);
  });

  it('.isGettable(key, obj) test property as gettable with object', () => {
    test.isGettable('otherProp', other);
  });

  it('.isGettable(key, obj) test getter/setter as gettable with object', () => {
    test.isGettable('getterSetterOtherProp', other);
  });

  it('.isGettable(key, obj) test getter as gettable with object', () => {
    test.isGettable('getterOtherProp', other);
  });

  it('.isNotGettable(key, obj) test setter as not a gettable with object', () => {
    test.isNotGettable('setterOtherProp', other);
  });

  it('.isNotGettable(key, obj) test method as not a gettable with object', () => {
    test.isNotGettable('otherMethod', other);
  });

  // #endregion

  // #region .isGetter(key, obj?)

  it('.isNotGetter(key) test non existant as not a getter', () => {
    test.isNotGetter('invalidGetter');
  });

  it('.isNotGetter(key) test property as not a getter', () => {
    test.isNotGetter('prop');
    test.isNotGetter('grandProp');
  });

  it('.isGetter(key) test getter/setter as getter', () => {
    test.isGetter('getterSetterProp');
    test.isGetter('getterSetterGrandProp');
  });

  it('.isGetter(key) test getter as getter', () => {
    test.isGetter('getterProp');
    test.isGetter('getterGrandProp');
  });

  it('.isNotGetter(key) test setter as not a getter', () => {
    test.isNotGetter('setterProp');
    test.isNotGetter('setterGrandProp');
  });

  it('.isNotGetter(key) test method as not a getter', () => {
    test.isNotGetter('method');
    test.isNotGetter('grandMethod');
  });

  it('.isNotGetter(key, obj) test non existant as not a getter with object', () => {
    test.isNotGetter('invalidGetter', other);
  });

  it('.isNotGetter(key, obj) test property as not a getter with object', () => {
    test.isNotGetter('otherProp', other);
  });

  it('.isGetter(key, obj) test getter/setter as getter with object', () => {
    test.isGetter('getterSetterOtherProp', other);
  });

  it('.isGetter(key, obj) test getter as getter with object', () => {
    test.isGetter('getterOtherProp', other);
  });

  it('.isNotGetter(key, obj) test setter as not a getter with object', () => {
    test.isNotGetter('setterOtherProp', other);
  });

  it('.isNotGetter(key, obj) test method as not a getter with object', () => {
    test.isNotGetter('otherMethod', other);
  });

  // #endregion

  // #region .isSettable(key, obj?)

  it('.isNotSettable(key) test non existant as not a settable', () => {
    test.isNotSettable('invalidSetter');
  });

  it('.isSettable(key) test property as settable', () => {
    test.isSettable('prop');
    test.isSettable('grandProp');
  });

  it('.isSettable(key) test getter/setter as settable', () => {
    test.isSettable('getterSetterProp');
    test.isSettable('getterSetterGrandProp');
  });

  it('.isNotSettable(key) test getter as not a settable', () => {
    test.isNotSettable('getterProp');
    test.isNotSettable('getterGrandProp');
  });

  it('.isSettable(key) test setter as settable', () => {
    test.isSettable('setterProp');
    test.isSettable('setterGrandProp');
  });

  it('.isNotSettable(key) test method as not a settable', () => {
    test.isNotSettable('method');
    test.isNotSettable('grandMethod');
  });

  it('.isNotSettable(key, obj) test non existant as not a settable with object', () => {
    test.isNotSettable('invalidGetter', other);
  });

  it('.isSettable(key, obj) test property as settable with object', () => {
    test.isSettable('otherProp', other);
  });

  it('.isSettable(key, obj) test getter/setter as settable with object', () => {
    test.isSettable('getterSetterOtherProp', other);
  });

  it('.isNotSettable(key, obj) test getter as not a settable with object', () => {
    test.isNotSettable('getterOtherProp', other);
  });

  it('.isSettable(key, obj) test setter as settable with object', () => {
    test.isSettable('setterOtherProp', other);
  });

  it('.isNotSettable(key, obj) test method as not a settable with object', () => {
    test.isNotSettable('otherMethod', other);
  });

  // #endregion

  // #region .isSetter(key, obj?)

  it('.isNotSetter(key) test non existant as not a setter', () => {
    test.isNotSetter('invalidSetter');
  });

  it('.isNotSetter(key) test property as not a setter', () => {
    test.isNotSetter('prop');
    test.isNotSetter('grandProp');
  });

  it('.isSetter(key) test getter/setter as setter', () => {
    test.isSetter('getterSetterProp');
    test.isSetter('getterSetterGrandProp');
  });

  it('.isNotSetter(key) test getter as not a setter', () => {
    test.isNotSetter('getterProp');
    test.isNotSetter('getterGrandProp');
  });

  it('.isSetter(key) test setter as setter', () => {
    test.isSetter('setterProp');
    test.isSetter('setterGrandProp');
  });

  it('.isNotSetter(key) test method as not a setter', () => {
    test.isNotSetter('method');
    test.isNotSetter('grandMethod');
  });

  it('.isNotSetter(key, obj) test non existant as not a setter with object', () => {
    test.isNotSetter('invalidSetter', other);
  });

  it('.isNotSetter(key, obj) test property as not a setter with object', () => {
    test.isNotSetter('otherProp', other);
  });

  it('.isSetter(key, obj) test getter/setter as setter with object', () => {
    test.isSetter('getterSetterOtherProp', other);
  });

  it('.isNotSetter(key, obj) test getter as not a setter with object', () => {
    test.isNotSetter('getterOtherProp', other);
  });

  it('.isSetter(key, obj) test setter as setter with object', () => {
    test.isSetter('setterOtherProp', other);
  });

  it('.isNotSetter(key, obj) test method as not a setter with object', () => {
    test.isNotSetter('otherMethod', other);
  });

  // #endregion

  // #region .isMethod(key, obj?)

  it('.isNotMethod(key) test non existant as not a method', () => {
    test.isNotMethod('invalidMethod');
  });

  it('.isNotMethod(key) test property as not a method', () => {
    test.isNotMethod('prop');
    test.isNotMethod('grandProp');
  });

  it('.isNotMethod(key) test getter/setter as not a method', () => {
    test.isNotMethod('getterSetterProp');
    test.isNotMethod('getterSetterGrandProp');
  });

  it('.isNotMethod(key) test getter as not a method', () => {
    test.isNotMethod('getterProp');
    test.isNotMethod('getterGrandProp');
  });

  it('.isNotMethod(key) test setter as not a method', () => {
    test.isNotMethod('setterProp');
    test.isNotMethod('setterGrandProp');
  });

  it('.isMethod(key) test method as method', () => {
    test.isMethod('method');
    test.isMethod('grandMethod');
  });

  it('.isNotMethod(key, obj) test non existant as not a method with object', () => {
    test.isNotMethod('invalidMethod', other);
  });

  it('.isNotMethod(key, obj) test property as not a method with object', () => {
    test.isNotMethod('otherProp', other);
  });

  it('.isNotMethod(key, obj) test getter/setter as not a method with object', () => {
    test.isNotMethod('getterSetterOtherProp', other);
  });

  it('.isNotMethod(key, obj) test getter as not a method with object', () => {
    test.isNotMethod('getterOtherProp', other);
  });

  it('.isNotMethod(key, obj) test setter as not a method with object', () => {
    test.isNotMethod('setterOtherProp', other);
  });

  it('.isMethod(key, obj) test method as method with object', () => {
    test.isMethod('otherMethod', other);
  });

  // #endregion

  // #region .hasValue(call, obj?)

  it('.hasValue({key, value}) test property has value', () => {
    testData.forEach((entry: TestEntry) => {
      test.obj.nullProp = entry[1];
      test.hasValue({ key: 'nullProp', value: entry[1] });
    });
  });

  it('.hasValue({key, value}, obj) test property has value with object', () => {
    testData.forEach((entry: TestEntry) => {
      other.nullOtherProp = entry[1];
      test.hasValue({ key: 'nullOtherProp', value: entry[1] }, other);
    });
  });

  // #endregion

  // #region .hasNotValue(call, obj?)

  it('.hasNotValue({key, value}) test property has not value', () => {
    test.hasNotValue({ key: 'prop', value: '' });
  });

  it('.hasNotValue({key, value}, obj) test property has not value with object', () => {
    test.hasNotValue({ key: 'otherProp', value: '' }, other);
  });

  // #endregion

  // #region .hasValueType(call, obj?)

  it('.hasValueType({key, typeOf}) test property has value type', () => {
    test.hasValueType({ key: 'prop', typeOf: 'string' });
  });

  it('.hasValueType({key, typeOf}, obj) test property has value type with object', () => {
    test.hasValueType({ key: 'otherProp', typeOf: 'string' }, other);
  });

  // #endregion

  // #region .hasNotValueType(call, obj?)

  it('.hasNotValueType({key, typeOf}) test property has not value type', () => {
    test.hasNotValueType({ key: 'prop', typeOf: 'object' });
  });

  it('.hasNotValueType({key, typeOf}, obj) test property has not value type with object', () => {
    test.hasNotValueType({ key: 'otherProp', typeOf: 'object' }, other);
  });

  // #endregion

  // #region .hasValueInstance(call, obj?)

  it('.hasValueInstance({key, instanceOf}) test property has value instance', () => {
    test.obj.nullProp = new LocalTestClass();
    test.hasValueInstance({ key: 'nullProp', instanceOf: LocalTestClass });
  });

  it('.hasValueInstance({key, instanceOf}, obj) test property has value instance with object', () => {
    other.nullOtherProp = new LocalTestClass();
    test.hasValueInstance({ key: 'nullOtherProp', instanceOf: LocalTestClass }, other);
  });

  // #endregion

  // #region .hasNotValueInstance(call, obj?)

  it('.hasNotValueInstance({key, instanceOf}) test property has not value instance', () => {
    test.obj.nullProp = new LocalTestClass();
    test.hasNotValueInstance({ key: 'nullProp', instanceOf: OtherTestClass });
  });

  it('.hasNotValueInstance({key, instanceOf}, obj) test property has not value instance with object', () => {
    other.nullOtherProp = new LocalTestClass();
    test.hasNotValueInstance({ key: 'nullOtherProp', instanceOf: OtherTestClass }, other);
  });

  // #endregion

  // #region .hasObservableValue(call, obj?)

  it('.hasObservableValue({key, value}) test property has Observable value', () => {
    test.obj.nullProp = of('a', asyncScheduler);
    test.hasObservableValue({ key: 'nullProp', value: 'a' });
  });

  it('.hasObservableValue({key, value}, obj) test property has Observable value with object', () => {
    other.nullOtherProp = of('a', asyncScheduler);
    test.hasObservableValue({ key: 'nullOtherProp', value: 'a' }, other);
  });

  // #endregion

  // #region .hasNotObservableValue(call, obj?)

  it('.hasNotObservableValue({key, value}) test property has not Observable value', () => {
    test.obj.nullProp = of('a', asyncScheduler);
    test.hasNotObservableValue({ key: 'nullProp', value: '' });
  });

  it('.hasNotObservableValue({key, value}, obj) test property has not Observable value with object', () => {
    other.nullOtherProp = of('a', asyncScheduler);
    test.hasNotObservableValue({ key: 'nullOtherProp', value: '' }, other);
  });

  // #endregion

  // #region .hasObservableValueType(call, obj?)

  it('.hasObservableValueType({key, value}) test property has Observable value type', () => {
    test.obj.nullProp = of('a', asyncScheduler);
    test.hasObservableValueType({ key: 'nullProp', typeOf: 'string' });
  });

  it('.hasObservableValueType({key, value}, obj) test property has Observable value type with object', () => {
    other.nullOtherProp = of('a', asyncScheduler);
    test.hasObservableValueType({ key: 'nullOtherProp', typeOf: 'string' }, other);
  });

  // #endregion

  // #region .hasNotObservableValueType(call, obj?)

  it('.hasNotObservableValueType({key, value}) test property has not Observable value type', () => {
    test.obj.nullProp = of('a', asyncScheduler);
    test.hasNotObservableValueType({ key: 'nullProp', typeOf: 'object' });
  });

  it('.hasNotObservableValueType({key, value}, obj) test property has not Observable value type with object', () => {
    other.nullOtherProp = of('a', asyncScheduler);
    test.hasNotObservableValueType({ key: 'nullOtherProp', typeOf: 'object' }, other);
  });

  // #endregion

  // #region .hasObservableValueInstance(call, obj?)

  it('.hasObservableValueInstance({key, instanceOf}) test property has Observable value instance', () => {
    test.obj.nullProp = of(new LocalTestClass(), asyncScheduler);
    test.hasObservableValueInstance({ key: 'nullProp', instanceOf: LocalTestClass });
  });

  it('.hasObservableValueInstance({key, instanceOf}, obj) test property has Observable value instance with object', () => {
    other.nullOtherProp = of(new LocalTestClass(), asyncScheduler);
    test.hasObservableValueInstance({ key: 'nullOtherProp', instanceOf: LocalTestClass }, other);
  });

  // #endregion

  // #region .hasNotObservableValueInstance(call, obj?)

  it('.hasNotObservableValueInstance({key, instanceOf}) test property has not Observable value instace type', () => {
    test.obj.nullProp = of(new LocalTestClass(), asyncScheduler);
    test.hasNotObservableValueInstance({ key: 'nullProp', instanceOf: OtherTestClass });
  });

  it('.hasNotObservableValueInstance({key, instanceOf}, obj) test property has not Observable value instance with object', () => {
    other.nullOtherProp = of(new LocalTestClass(), asyncScheduler);
    test.hasNotObservableValueInstance({ key: 'nullOtherProp', instanceOf: OtherTestClass }, other);
  });

  // #endregion

  // #region .hasObservableError(call, obj?)

  it('.hasObservableError({key, error}) test property has Observable error', () => {
    test.obj.nullProp = throwError('a', asyncScheduler);
    test.hasObservableError({ key: 'nullProp', error: 'a' });
  });

  it('.hasObservableError({key, error}, obj) test property has Observable error with object', () => {
    other.nullOtherProp = throwError('a', asyncScheduler);
    test.hasObservableError({ key: 'nullOtherProp', error: 'a' }, other);
  });

  // #endregion

  // #region .hasNotObservableError(call, obj?)

  it('.hasNotObservableError({key, error}) test property has not Observable error', () => {
    test.obj.nullProp = throwError('a', asyncScheduler);
    test.hasNotObservableError({ key: 'nullProp', error: '' });
  });

  it('.hasNotObservableError({key, error}, obj) test property has not Observable error with object', () => {
    other.nullOtherProp = throwError('a', asyncScheduler);
    test.hasNotObservableError({ key: 'nullOtherProp', error: '' }, other);
  });

  // #endregion

  // #region .hasObservableErrorType(call, obj?)

  it('.hasObservableErrorType({key, typeOf}) test property has Observable error type', () => {
    test.obj.nullProp = throwError('a', asyncScheduler);
    test.hasObservableErrorType({ key: 'nullProp', typeOf: 'string' });
  });

  it('.hasObservableErrorType({key, typeOf}, obj) test property has Observable error type with object', () => {
    other.nullOtherProp = throwError('a', asyncScheduler);
    test.hasObservableErrorType({ key: 'nullOtherProp', typeOf: 'string' }, other);
  });

  // #endregion

  // #region .hasNotObservableErrorType(call, obj?)

  it('.hasNotObservableErrorType({key, typeOf}) test property has not Observable error type', () => {
    test.obj.nullProp = throwError('a', asyncScheduler);
    test.hasNotObservableErrorType({ key: 'nullProp', typeOf: 'object' });
  });

  it('.hasNotObservableErrorType({key, typeOf}, obj) test property has not Observable error type with object', () => {
    other.nullOtherProp = throwError('a', asyncScheduler);
    test.hasNotObservableErrorType({ key: 'nullOtherProp', typeOf: 'object' }, other);
  });

  // #endregion

  // #region .hasObservableErrorInstance(call, obj?)

  it('.hasObservableErrorInstance({key, instanceOf}) test property has Observable error instance', () => {
    test.obj.nullProp = throwError(new LocalTestClass(), asyncScheduler);
    test.hasObservableErrorInstance({ key: 'nullProp', instanceOf: LocalTestClass });
  });

  it('.hasObservableErrorInstance({key, instanceOf}, obj) test property has Observable error instance with object', () => {
    other.nullOtherProp = throwError(new LocalTestClass(), asyncScheduler);
    test.hasObservableErrorInstance({ key: 'nullOtherProp', instanceOf: LocalTestClass }, other);
  });

  // #endregion

  // #region .hasNotObservableErrorInstance(call, obj?)

  it('.hasNotObservableErrorInstance({key, instanceOf}) test property has not Observable error instance', () => {
    test.obj.nullProp = throwError(new LocalTestClass(), asyncScheduler);
    test.hasNotObservableErrorInstance({ key: 'nullProp', instanceOf: OtherTestClass });
  });

  it('.hasNotObservableErrorInstance({key, instanceOf}, obj) test property has not Observable error instance with object', () => {
    other.nullOtherProp = throwError(new LocalTestClass(), asyncScheduler);
    test.hasNotObservableErrorInstance({ key: 'nullOtherProp', instanceOf: OtherTestClass }, other);
  });

  // #endregion

  // #region .hasPromiseValue(call, obj?)

  it('.hasPromiseValue({key, value}) test property has Promise value', () => {
    test.obj.nullProp = Promise.resolve('a');
    test.hasPromiseValue({ key: 'nullProp', value: 'a' });
  });

  it('.hasPromiseValue({key, value}, obj) test property has Promise value with object', () => {
    other.nullOtherProp = Promise.resolve('a');
    test.hasPromiseValue({ key: 'nullOtherProp', value: 'a' }, other);
  });

  // #endregion

  // #region .hasNotPromiseValue(call, obj?)

  it('.hasNotPromiseValue({key, value}) test property has not Promise value', () => {
    test.obj.nullProp = Promise.resolve('a');
    test.hasNotPromiseValue({ key: 'nullProp', value: 'notAValue' });
  });

  it('.hasNotPromiseValue({key, value}, obj) test property has not Promise value with object', () => {
    other.nullOtherProp = Promise.resolve('a');
    test.hasNotPromiseValue({ key: 'nullOtherProp', value: 'notAValue' }, other);
  });

  // #endregion

  // #region .hasPromiseValueType(call, obj?)

  it('.hasPromiseValueType({key, typeOf}) test property has Promise value type', () => {
    test.obj.nullProp = Promise.resolve('a');
    test.hasPromiseValueType({ key: 'nullProp', typeOf: 'string' });
  });

  it('.hasPromiseValueType({key, typeOf}, obj) test property has Promise value type with object', () => {
    other.nullOtherProp = Promise.resolve('a');
    test.hasPromiseValueType({ key: 'nullOtherProp', typeOf: 'string' }, other);
  });

  // #endregion

  // #region .hasNotPromiseValueType(call, obj?)

  it('.hasNotPromiseValueType({key, typeOf}) test property has not Promise value type', () => {
    test.obj.nullProp = Promise.resolve('a');
    test.hasNotPromiseValueType({ key: 'nullProp', typeOf: 'object' });
  });

  it('.hasNotPromiseValueType({key, typeOf}, obj) test property has not Promise value type with object', () => {
    other.nullOtherProp = Promise.resolve('a');
    test.hasNotPromiseValueType({ key: 'nullOtherProp', typeOf: 'object' }, other);
  });

  // #endregion

  // #region .hasPromiseValueInstance(call, obj?)

  it('.hasPromiseValueInstance({key, instanceOf}) test property has Promise value instance', () => {
    test.obj.nullProp = Promise.resolve(new LocalTestClass());
    test.hasPromiseValueInstance({ key: 'nullProp', instanceOf: LocalTestClass });
  });

  it('.hasPromiseValueInstance({key, instanceOf}, obj) test property has Promise value instance with object', () => {
    other.nullOtherProp = Promise.resolve(new LocalTestClass());
    test.hasPromiseValueInstance({ key: 'nullOtherProp', instanceOf: LocalTestClass }, other);
  });

  // #endregion

  // #region .hasNotPromiseValueInstance(call, obj?)

  it('.hasNotPromiseValueInstance({key, instanceOf}) test property has not Promise value instance', () => {
    test.obj.nullProp = Promise.resolve(new LocalTestClass());
    test.hasNotPromiseValueInstance({ key: 'nullProp', instanceOf: OtherTestClass });
  });

  it('.hasNotPromiseValueInstance({key, instanceOf}, obj) test property has not Promise value instance with object', () => {
    other.nullOtherProp = Promise.resolve(new LocalTestClass());
    test.hasNotPromiseValueInstance({ key: 'nullOtherProp', instanceOf: OtherTestClass }, other);
  });

  // #endregion

  // #region .hasPromiseError(call, obj?)

  it('.hasPromiseError({key, error}) test property has Promise error', () => {
    test.obj.nullProp = Promise.reject('a');
    test.hasPromiseError({ key: 'nullProp', error: 'a' });
  });

  it('.hasPromiseError({key, error}, obj) test property has Promise error with object', () => {
    other.nullOtherProp = Promise.reject('a');
    test.hasPromiseError({ key: 'nullOtherProp', error: 'a' }, other);
  });

  // #endregion

  // #region .hasNotPromiseError(call, obj?)

  it('.hasNotPromiseError({key, error}) test property has not Promise error', () => {
    test.obj.nullProp = Promise.reject('a');
    test.hasNotPromiseError({ key: 'nullProp', error: 'notAValue' });
  });

  it('.hasNotPromiseError({key, error}, obj) test property has not Promise error with object', () => {
    other.nullOtherProp = Promise.reject('a');
    test.hasNotPromiseError({ key: 'nullOtherProp', error: 'notAValue' }, other);
  });

  // #endregion

  // #region .hasPromiseErrorType(call, obj?)

  it('.hasPromiseErrorType({key, typeOf}) test property has Promise error type', () => {
    test.obj.nullProp = Promise.reject('a');
    test.hasPromiseErrorType({ key: 'nullProp', typeOf: 'string' });
  });

  it('.hasPromiseErrorType({key, typeOf}, obj) test property has Promise error type with object', () => {
    other.nullOtherProp = Promise.reject('a');
    test.hasPromiseErrorType({ key: 'nullOtherProp', typeOf: 'string' }, other);
  });

  // #endregion

  // #region .hasNotPromiseErrorType(call, obj?)

  it('.hasNotPromiseErrorType({key, typeOf}) test property has not Promise error type', () => {
    test.obj.nullProp = Promise.reject('a');
    test.hasNotPromiseErrorType({ key: 'nullProp', typeOf: 'object' });
  });

  it('.hasNotPromiseErrorType({key, typeOf}, obj) test property has not Promise error type with object', () => {
    other.nullOtherProp = Promise.reject('a');
    test.hasNotPromiseErrorType({ key: 'nullOtherProp', typeOf: 'object' }, other);
  });

  // #endregion

  // #region .hasPromiseErrorInstance(call, obj?)

  it('.hasPromiseErrorInstance({key, instanceOf}) test property has Promise error instance', () => {
    test.obj.nullProp = Promise.reject(new LocalTestClass());
    test.hasPromiseErrorInstance({ key: 'nullProp', instanceOf: LocalTestClass });
  });

  it('.hasPromiseErrorInstance({key, instanceOf}, obj) test property has Promise error instance with object', () => {
    other.nullOtherProp = Promise.reject(new LocalTestClass());
    test.hasPromiseErrorInstance({ key: 'nullOtherProp', instanceOf: LocalTestClass }, other);
  });

  // #endregion

  // #region .hasNotPromiseErrorInstance(call, obj?)

  it('.hasNotPromiseErrorInstance({key, instanceOf}) test property has Promise error instance', () => {
    test.obj.nullProp = Promise.reject(new LocalTestClass());
    test.hasNotPromiseErrorInstance({ key: 'nullProp', instanceOf: OtherTestClass });
  });

  it('.hasNotPromiseErrorInstance({key, instanceOf}, obj) test property has Promise error instance with object', () => {
    other.nullOtherProp = Promise.reject(new LocalTestClass());
    test.hasNotPromiseErrorInstance({ key: 'nullOtherProp', instanceOf: OtherTestClass }, other);
  });

  // #endregion

  // #region .returnsValue(call, obj?)

  it('.returnsValue({key, value}) test method returns value', () => {
    testData.forEach((entry: TestEntry) => {
      test.returnsValue({ key: 'method', value: entry[1], args: [entry[1]] });
    });
  });

  it('.returnsValue({key, value}, obj) test method returns value with object', () => {
    testData.forEach((entry: TestEntry) => {
      test.returnsValue({ key: 'otherMethod', value: entry[1], args: [entry[1]] }, other);
    });
  });

  // #endregion

  // #region .returnsNotValue(call, obj?)

  it('.returnsNotValue({key, value}) test method do not returns value', () => {
    testData.forEach((entry: TestEntry) => {
      test.returnsNotValue({ key: 'method', value: 'notAValue', args: [entry[1]] });
    });
  });

  it('.returnsNotValue({key, value, args}, obj) test method do not returns value with object', () => {
    testData.forEach((entry: TestEntry) => {
      test.returnsNotValue({ key: 'otherMethod', value: 'notAValue', args: [entry[1]] }, other);
    });
  });

  // #endregion

  // #region .returnsValueType(call, obj?)

  it('.returnsValueType({key, typeOf}) test method returns value type', () => {
    test.returnsValueType({ key: 'method', typeOf: 'string', args: ['a'] });
  });

  it('.returnsValueType({key, typeOf}, obj) test method returns value type with object', () => {
    test.returnsValueType({ key: 'otherMethod', typeOf: 'string', args: ['a'] }, other);
  });

  // #endregion

  // #region .returnsNotValueType(call, obj?)

  it('.returnsNotValueType({key, typeOf}) test method returns not value type', () => {
    test.returnsNotValueType({ key: 'method', typeOf: 'object', args: ['a'] });
  });

  it('.returnsNotValueType({key, typeOf}, obj) test method returns not value type with object', () => {
    test.returnsNotValueType({ key: 'otherMethod', typeOf: 'object', args: ['a'] }, other);
  });

  // #endregion

  // #region .returnsValueInstance(call, obj?)

  it('.returnsValueInstance({key, instanceOf}) test method returns value instance', () => {
    test.returnsValueInstance({
      key: 'method',
      instanceOf: LocalTestClass,
      args: [new LocalTestClass()]
    });
  });

  it('.returnsValueInstance({key, instanceOf}, obj) test method returns value instance with object', () => {
    test.returnsValueInstance(
      { key: 'otherMethod', instanceOf: LocalTestClass, args: [new LocalTestClass()] },
      other
    );
  });

  // #endregion

  // #region .returnsNotValueInstance(call, obj?)

  it('.returnsNotValueInstance({key, instanceOf}) test method do not returns value instance', () => {
    test.returnsNotValueInstance({
      key: 'method',
      instanceOf: OtherTestClass,
      args: [new LocalTestClass()]
    });
  });

  it('.returnsNotValueInstance({key, instanceOf}, obj) test method do not returns value instance with object', () => {
    test.returnsNotValueInstance(
      { key: 'otherMethod', instanceOf: OtherTestClass, args: [new LocalTestClass()] },
      other
    );
  });

  // #endregion

  // #region .returnsObservableValue(call, obj?)

  it('.returnsObservableValue({key, value}) test method returns Observable value', () => {
    test.returnsObservableValue({ key: 'methodObservable', value: 'a', args: ['a'] });
  });

  it('.returnsObservableValue({key, value}, obj) test method returns Observable value with object', () => {
    test.returnsObservableValue({ key: 'otherMethodObservable', value: 'a', args: ['a'] }, other);
  });

  // #endregion

  // #region .returnsNotObservableValue(call, obj?)

  it('.returnsNotObservableValue({key, value}) test method returns not Observable', () => {
    test.returnsNotObservableValue({ key: 'methodObservable', value: '' });
  });

  it('.returnsNotObservableValue({key, value}, obj) test method returns not Observable value with object', () => {
    test.returnsNotObservableValue({ key: 'otherMethodObservable', value: '', args: ['a'] }, other);
  });

  // #endregion

  // #region .returnsObservableValueType(call, obj?)

  it('.returnsObservableValueType({key, typeOf}) test method returns Observable value type', () => {
    test.returnsObservableValueType({ key: 'methodObservable', typeOf: 'string', args: ['a'] });
  });

  it('.returnsObservableValueType({key, typeOf}, obj) test method returns Observable value type with object', () => {
    test.returnsObservableValueType(
      { key: 'otherMethodObservable', typeOf: 'string', args: ['a'] },
      other
    );
  });

  // #endregion

  // #region .returnsNotObservableValueType(call, obj?)

  it('.returnsNotObservableValueType({key, typeOf}) test method returns not Observable value type', () => {
    test.returnsNotObservableValueType({ key: 'methodObservable', typeOf: 'object', args: ['a'] });
  });

  it('.returnsNotObservableValueType({key, typeOf}, obj) test method returns not Observable value type with object', () => {
    test.returnsNotObservableValueType(
      { key: 'otherMethodObservable', typeOf: 'object', args: ['a'] },
      other
    );
  });

  // #endregion

  // #region .returnsObservableValueInstance(call, obj?)

  it('.returnsObservableValueInstance({key, instanceOf}) test method returns Observable value instance', () => {
    test.returnsObservableValueInstance({
      key: 'methodObservable',
      instanceOf: LocalTestClass,
      args: [new LocalTestClass()]
    });
  });

  it('.returnsObservableValueInstance({key, instanceOf}, obj) test method returns Observable value instance with object', () => {
    test.returnsObservableValueInstance(
      { key: 'otherMethodObservable', instanceOf: LocalTestClass, args: [new LocalTestClass()] },
      other
    );
  });

  // #endregion

  // #region .returnsNotObservableValueInstance(call, obj?)

  it('.returnsNotObservableValueInstance({key, instanceOf}) test method returns Observable value instance', () => {
    test.returnsNotObservableValueInstance({
      key: 'methodObservable',
      instanceOf: OtherTestClass,
      args: [new LocalTestClass()]
    });
  });

  it('.returnsNotObservableValueInstance({key, instanceOf}, obj) test method returns Observable value instance with object', () => {
    test.returnsNotObservableValueInstance(
      { key: 'otherMethodObservable', instanceOf: OtherTestClass, args: [new LocalTestClass()] },
      other
    );
  });

  // #endregion

  // #region .returnsObservableError(call, obj?)

  it('.returnsObservableError({key, error}) test method returns Observable error', () => {
    test.returnsObservableError({
      key: 'method',
      error: 'a',
      args: [throwError('a', asyncScheduler)]
    });
  });

  it('.returnsObservableError({key, error}, obj) test method returns Observable error with object', () => {
    test.returnsObservableError(
      { key: 'otherMethod', error: 'a', args: [throwError('a', asyncScheduler)] },
      other
    );
  });

  // #endregion

  // #region .returnsNotObservableError(call, obj?)

  it('.returnsNotObservableError({key, error}) test method returns not Observable error', () => {
    test.returnsNotObservableError({
      key: 'method',
      error: '',
      args: [throwError('a', asyncScheduler)]
    });
  });

  it('.returnsNotObservableError({key, error, args}, obj) test method returns not Observable error with object', () => {
    test.returnsNotObservableError(
      { key: 'otherMethod', error: '', args: [throwError('a', asyncScheduler)] },
      other
    );
  });

  // #endregion

  // #region .returnsObservableErrorType(call, obj?)

  it('.returnsObservableErrorType({key, typeOf}) test method returns Observable error type', () => {
    test.returnsObservableErrorType({
      key: 'method',
      typeOf: 'string',
      args: [throwError('a', asyncScheduler)]
    });
  });

  it('.returnsObservableErrorType({key, typeOf}, obj) test method returns Observable error type with object', () => {
    test.returnsObservableErrorType(
      { key: 'otherMethod', typeOf: 'string', args: [throwError('a', asyncScheduler)] },
      other
    );
  });

  // #endregion

  // #region .returnsNotObservableErrorType(call, obj?)

  it('.returnsNotObservableErrorType({key, typeOf}) test method do not returns Observable error type', () => {
    test.returnsNotObservableErrorType({
      key: 'method',
      typeOf: 'object',
      args: [throwError('a', asyncScheduler)]
    });
  });

  it('.returnsNotObservableErrorType({key, typeOf}, obj) test method do not returns Observable error type with object', () => {
    test.returnsNotObservableErrorType(
      { key: 'otherMethod', typeOf: 'object', args: [throwError('a', asyncScheduler)] },
      other
    );
  });

  // #endregion

  // #region .returnsObservableErrorInstance(call, obj?)

  it('.returnsObservableErrorInstance({key, instanceOf}) test method returns Observable error instance', () => {
    test.returnsObservableErrorInstance({
      key: 'method',
      instanceOf: LocalTestClass,
      args: [throwError(new LocalTestClass(), asyncScheduler)]
    });
  });

  it('.returnsObservableErrorInstance({key, instanceOf}, obj) test method returns Observable error instance with object', () => {
    test.returnsObservableErrorInstance(
      {
        key: 'otherMethod',
        instanceOf: LocalTestClass,
        args: [throwError(new LocalTestClass(), asyncScheduler)]
      },
      other
    );
  });

  // #endregion

  // #region .returnsNotObservableErrorInstance(call, obj?)

  it('.returnsNotObservableErrorInstance({key, instanceOf}) test method returns Observable error instance', () => {
    test.returnsNotObservableErrorInstance({
      key: 'method',
      instanceOf: OtherTestClass,
      args: [throwError(new LocalTestClass(), asyncScheduler)]
    });
  });

  it('.returnsNotObservableErrorInstance({key, instanceOf}, obj) test method returns Observable error instance with object', () => {
    test.returnsNotObservableErrorInstance(
      {
        key: 'otherMethod',
        instanceOf: OtherTestClass,
        args: [throwError(new LocalTestClass(), asyncScheduler)]
      },
      other
    );
  });

  // #endregion

  // #region .returnsPromiseValue(call, obj?)

  it('.returnsPromiseValue({key, value}) test method returns Promise value', () => {
    test.returnsPromiseValue({ key: 'methodPromise', value: 'a', args: ['a'] });
  });

  it('.returnsPromiseValue({key, value}, obj) test method returns Promise value with object', () => {
    test.returnsPromiseValue({ key: 'otherMethodPromise', value: 'a', args: ['a'] }, other);
  });

  // #endregion

  // #region .returnsNotPromiseValue(call, obj?)

  it('.returnsNotPromiseValue({key, value}) test method returns not Promise value', () => {
    test.returnsNotPromiseValue({ key: 'methodPromise', value: '', args: ['a'] });
  });

  it('.returnsNotPromiseValue({key, value}, obj) test method returns not Promise value with object', () => {
    test.returnsNotPromiseValue({ key: 'otherMethodPromise', value: '', args: ['a'] }, other);
  });

  // #endregion

  // #region .returnsPromiseValueType(call, obj?)

  it('.returnsPromiseValueType({key, typeOf}) test method returns Promise value type', () => {
    test.returnsPromiseValueType({ key: 'methodPromise', typeOf: 'string', args: ['a'] });
  });

  it('.returnsPromiseValueType({key, typeOf}, obj) test method returns Promise value type with object', () => {
    test.returnsPromiseValueType(
      { key: 'otherMethodPromise', typeOf: 'string', args: ['a'] },
      other
    );
  });

  // #endregion

  // #region .returnsNotPromiseValueType(call, obj?)

  it('.returnsNotPromiseValueType({key, typeOf}) test method do not returns Promise value type', () => {
    test.returnsNotPromiseValueType({ key: 'methodPromise', typeOf: 'object', args: ['a'] });
  });

  it('.returnsNotPromiseValueType({key, typeOf}, obj) test method do not returns Promise value type with object', () => {
    test.returnsNotPromiseValueType(
      { key: 'otherMethodPromise', typeOf: 'object', args: ['a'] },
      other
    );
  });

  // #endregion

  // #region .returnsPromiseValueInstance(call, obj?)

  it('.returnsPromiseValueInstance({key, instanceOf}) test method returns Promise value instance', () => {
    test.returnsPromiseValueInstance({
      key: 'methodPromise',
      instanceOf: LocalTestClass,
      args: [new LocalTestClass()]
    });
  });

  it('.returnsPromiseValueInstance({key, instanceOf}, obj) test method returns Promise value instance with object', () => {
    test.returnsPromiseValueInstance(
      { key: 'otherMethodPromise', instanceOf: LocalTestClass, args: [new LocalTestClass()] },
      other
    );
  });

  // #endregion

  // #region .returnsNotPromiseValueInstance(call, obj?)

  it('.returnsNotPromiseValueInstance({key, instanceOf}) test method do not returns Promise value instance', () => {
    test.returnsNotPromiseValueInstance({
      key: 'methodPromise',
      instanceOf: OtherTestClass,
      args: [new LocalTestClass()]
    });
  });

  it('.returnsNotPromiseValueInstance({key, instanceOf}, obj) test method do not returns Promise value instance with object', () => {
    test.returnsNotPromiseValueInstance(
      { key: 'otherMethodPromise', instanceOf: OtherTestClass, args: [new LocalTestClass()] },
      other
    );
  });

  // #endregion

  // #region .returnsPromiseError(call, obj?)

  it('.returnsPromiseError({key, error}) test method returns Promise error', () => {
    test.returnsPromiseError({ key: 'method', error: 'a', args: [Promise.reject('a')] });
  });

  it('.returnsPromiseError({key, error}, obj) test method returns Promise error with object', () => {
    test.returnsPromiseError(
      { key: 'otherMethod', error: 'a', args: [Promise.reject('a')] },
      other
    );
  });

  // #endregion

  // #region .returnsNotPromiseError(call, obj?)

  it('.returnsNotPromiseError({key, error}) test method returns not Promise error', () => {
    test.returnsNotPromiseError({ key: 'method', error: '', args: [Promise.reject('a')] });
  });

  it('.returnsNotPromiseError({key, error}, obj) test method returns not Promise error with object', () => {
    test.returnsNotPromiseError(
      { key: 'otherMethod', error: '', args: [Promise.reject('a')] },
      other
    );
  });

  // #endregion

  // #region .returnsPromiseErrorType(call, obj?)

  it('.returnsPromiseErrorType({key, typeOf}) test method returns Promise error type', () => {
    test.returnsPromiseErrorType({ key: 'method', typeOf: 'string', args: [Promise.reject('a')] });
  });

  it('.returnsPromiseErrorType({key, typeOf}, obj) test method returns Promise error type with object', () => {
    test.returnsPromiseErrorType(
      { key: 'otherMethod', typeOf: 'string', args: [Promise.reject('a')] },
      other
    );
  });

  // #endregion

  // #region .returnsNotPromiseErrorType(call, obj?)

  it('.returnsNotPromiseErrorType({key, typeOf}) test method do not returns Promise error type', () => {
    test.returnsNotPromiseErrorType({
      key: 'method',
      typeOf: 'object',
      args: [Promise.reject('a')]
    });
  });

  it('.returnsNotPromiseErrorType({key, typeOf}, obj) test method do not returns Promise error type with object', () => {
    test.returnsNotPromiseErrorType(
      { key: 'otherMethod', typeOf: 'object', args: [Promise.reject('a')] },
      other
    );
  });

  // #endregion

  // #region .returnsPromiseErrorInstance(call, obj?)

  it('.returnsPromiseErrorInstance({key, instanceOf}) test method returns Promise error instance', () => {
    test.returnsPromiseErrorInstance({
      key: 'method',
      instanceOf: LocalTestClass,
      args: [Promise.reject(new LocalTestClass())]
    });
  });

  it('.returnsPromiseErrorInstance({key, instanceOf}, obj) test method returns Promise error instance with object', () => {
    test.returnsPromiseErrorInstance(
      {
        key: 'otherMethod',
        instanceOf: LocalTestClass,
        args: [Promise.reject(new LocalTestClass())]
      },
      other
    );
  });

  // #endregion

  // #region .returnsNotPromiseErrorInstance(call, obj?)

  it('.returnsNotPromiseErrorInstance({key, instanceOf}) test method do not returns Promise error instance', () => {
    test.returnsNotPromiseErrorInstance({
      key: 'method',
      instanceOf: OtherTestClass,
      args: [Promise.reject(new LocalTestClass())]
    });
  });

  it('.returnsNotPromiseErrorInstance({key, instanceOf}, obj) test method do not returns Promise error instance with object', () => {
    test.returnsNotPromiseErrorInstance(
      {
        key: 'otherMethod',
        instanceOf: OtherTestClass,
        args: [Promise.reject(new LocalTestClass())]
      },
      other
    );
  });

  // #endregion

  // #region .consoleIsCalled(call, obj?)

  it('.consoleIsCalled({level, args, fromKey, fromArgs?}) test a method have been called', () => {
    test.consoleIsCalled({
      level: 'log',
      args: ['test'],
      fromKey: 'consoleLog',
      fromArgs: ['test']
    });
  });

  // #endregion

  // #region .consoleIsNotCalled(call, obj?)

  it('.consoleIsNotCalled({level, args, fromKey, fromArgs?}) test a method have not been called', () => {
    test.consoleIsNotCalled({
      level: 'log',
      args: ['test'],
      fromKey: 'consoleLog',
      fromArgs: ['test2']
    });
  });

  // #endregion

  // #region .methodIsCalled(call, obj?)

  it('.methodIsCalled({key, fromKey}) test a method have been called', () => {
    test.methodIsCalled({ key: 'privateMethod', fromKey: 'callMethod' });
  });

  it('.methodIsCalled({key, args, fromKey}) test a method have been called with args', () => {
    test.methodIsCalled({
      key: 'privateMethod',
      args: ['a'],
      fromKey: 'callMethod',
      fromArgs: ['a']
    });
  });

  it('.methodIsCalled({key, fromKey, callObj}) test a secondary object method have been called', () => {
    test.methodIsCalled({
      key: 'otherMethod',
      fromKey: 'fireOther',
      spiedObj: otherObj
    });
  });

  it('.methodIsCalled({key, fromKey, callObj}) test a secondary object method have been called with fromArgs', () => {
    const event: Event = new Event('a');

    test.methodIsCalled({
      key: 'stopPropagation',
      fromKey: 'fireEvent',
      fromArgs: [event],
      spiedObj: event
    });
  });

  it('.methodIsCalled({key, fromKey}, obj) test a method have been called with object', () => {
    test.methodIsCalled({ key: 'otherPrivateMethod', fromKey: 'otherCallMethod' }, other);
  });

  it('.methodIsCalled({key, args, fromKey, fromArgs}, obj) test a method have been called with args and object', () => {
    test.methodIsCalled(
      {
        key: 'otherPrivateMethod',
        args: ['a'],
        fromKey: 'otherCallMethod',
        fromArgs: ['a']
      },
      other
    );
  });

  // #endregion

  // #region .methodIsNotCalled(call, obj?)

  it('.methodIsNotCalled({key, fromKey}) test a method have not been called', () => {
    test.methodIsNotCalled({ key: 'privateMethod', fromKey: 'method' });
  });

  it('.methodIsNotCalled({key, args, fromKey}) test a method have not been called with args', () => {
    test.methodIsNotCalled({ key: 'privateMethod', args: ['a'], fromKey: 'method' });
  });

  it('.methodIsNotCalled({key, fromKey, obj}) test a secondary object method have been called', () => {
    const event: Event = new Event('a');

    test.methodIsNotCalled({
      key: 'preventDefault',
      fromKey: 'fireEvent',
      fromArgs: [event],
      spiedObj: event
    });
  });

  it('.methodIsNotCalled({key, fromKey}, obj) test a method have not been called with object', () => {
    test.methodIsNotCalled({ key: 'otherPrivateMethod', fromKey: 'otherMethod' }, other);
  });

  // #endregion

  // #region .getPropValue(call, obj)

  it('.getPropValue({... key}, obj) get value from property', () => {
    test.obj.nullProp = 'c';
    test.hasValue({ key: 'nullProp', value: 'c' });
  });

  it('.getPropValue({... key}, obj) get value from getter/setter', () => {
    test.obj.getterSetterProp = 'c';
    test.hasValue({ key: 'getterSetterProp', value: 'c' });
  });

  it('.getPropValue({... key}, obj) get value from getter', () => {
    test.obj.getterSetterProp = 'c';
    test.hasValue({ key: 'getterProp', value: 'c' });
  });

  // #endregion

  // #region .getMethodValue(call, obj)

  it('.getMethodValue({... key}, obj) get value from method', () => {
    test.returnsValue({ key: 'method', value: undefined });
  });

  it('.getMethodValue({... key, args}, obj) get value from method with args', () => {
    test.returnsValue({ key: 'method', value: 'a', args: ['a'] });
  });

  // #endregion
});
