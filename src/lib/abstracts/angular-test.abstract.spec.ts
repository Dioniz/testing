import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SpyLocation } from '@angular/common/testing';
import { Component, Injectable } from '@angular/core';
import { Router, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TestBedConfig, TestClass } from '@ngxa/utils';

import { AngularTestAbstract } from './angular-test.abstract';

const notFound: string = 'notFoundValue';

class AngularTest extends AngularTestAbstract<TestClass> {}

@Injectable()
class TestService {}

@Component({
  selector: 'ngxa-test',
  template: ''
})
class TestComponent {
  constructor(protected service: TestService, protected router: Router) {}

  public navigate(url: any[]): void {
    this.router.navigate(url);
  }
}

class AngularComponentTest extends AngularTestAbstract<TestComponent> {}

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: TestComponent },
  { path: 'search', component: TestComponent },
  { path: 'search/:id', component: TestComponent, data: { test: 'test' } }
];

describe('AngularTestAbstract', () => {
  let test: AngularTest | undefined;

  beforeEach(() => {
    test = undefined;
  });

  it('.testBed is setted with a basic config', () => {
    test = new AngularTest(TestClass);
    expect(test.testBed).toBeDefined();
    expect(test.testBed.get(CommonModule, notFound)).not.toEqual(notFound);
    expect(test.testBed.get(HttpClientModule, notFound)).toEqual(notFound);
  });

  it('.testBed is setted with a custom config', () => {
    const testBedConfig: TestBedConfig = new TestBedConfig();

    testBedConfig.imports.push(HttpClientModule);
    test = new AngularTest(TestClass, testBedConfig);
    expect(test.testBed).toBeDefined();
    expect(test.testBed.get(CommonModule, notFound)).not.toEqual(notFound);
    expect(test.testBed.get(HttpClientModule, notFound)).not.toEqual(notFound);
  });

  it('.isImported check that a module is imported', () => {
    const testBedConfig: TestBedConfig = new TestBedConfig();

    testBedConfig.imports.push(HttpClientModule);
    test = new AngularTest(TestClass, testBedConfig);
    test.isImported(HttpClientModule);
  });

  it('.isNotImported check that a module is not imported', () => {
    const testBedConfig: TestBedConfig = new TestBedConfig();

    test = new AngularTest(TestClass, testBedConfig);
    test.isNotImported(HttpClientModule);
  });

  it('.isComponentDeclared check that a component is declared', () => {
    const testBedConfig: TestBedConfig = new TestBedConfig();

    testBedConfig.declarations.push(TestComponent);
    testBedConfig.providers.push(TestService);
    test = new AngularTest(TestClass, testBedConfig);
    test.isComponentDeclared(TestComponent);
  });

  it('.isNotComponentDeclared check that a component is not declared', () => {
    const testBedConfig: TestBedConfig = new TestBedConfig();

    test = new AngularTest(TestClass, testBedConfig);
    test.isNotComponentDeclared(TestComponent);
  });

  it('.isProvided check that a service is provided', () => {
    const testBedConfig: TestBedConfig = new TestBedConfig();

    testBedConfig.imports.push(HttpClientModule);
    test = new AngularTest(TestClass, testBedConfig);
    test.isProvided(HttpClient);
  });

  it('.isNotProvided check that a service is not provided', () => {
    const testBedConfig: TestBedConfig = new TestBedConfig();

    test = new AngularTest(TestClass, testBedConfig);
    test.isNotProvided(HttpClient);
  });

  describe('', () => {
    let test: AngularComponentTest;
    const testBedConfig: TestBedConfig = new TestBedConfig();

    beforeEach(() => {
      testBedConfig.imports.push(HttpClientModule);
      testBedConfig.declarations.push(TestComponent);
      testBedConfig.providers.push(TestService);
      test = new AngularComponentTest(TestComponent, testBedConfig, routes);
      test.obj = test.testBed.createComponent(TestComponent).componentInstance;
    });

    it('.testBed have imported RouterTestingModule', () => {
      test.isImported(RouterTestingModule);
    });

    it('.location is setted with the SpyLocation service', () => {
      expect(test.location).toBeDefined();
      expect(test.location instanceof SpyLocation).toBeTruthy();
    });

    it('.router is setted with the Router service', () => {
      expect(test.router).toBeDefined();
      expect(test.router instanceof Router).toBeTruthy();
    });

    it('.router is initialized', () => {
      expect(test.router.url).toEqual('/');
    });

    it('.isInjected(class) check that a service is injected', () => {
      test.isInjected(TestService);
    });

    it('.isNotInjected(class) check that a service is not injected', () => {
      test.isNotInjected(HttpClient);
    });

    it('.navigateOnCall({key, args?, url}) navigate to directly', () => {
      test.navigateOnCall({ key: 'navigate', args: [['/search']], url: '/search' });
    });

    it('.navigateOnCall({key, args?, url}) navigate to with redirections', () => {
      test.navigateOnCall({ key: 'navigate', args: [['/']], url: '/home' });
    });

    it('.notNavigateOnCall({key, args?, url}) do not navigate to', () => {
      test.notNavigateOnCall({ key: 'navigate', args: [['/search']], url: '/home' });
    });
  });
});
