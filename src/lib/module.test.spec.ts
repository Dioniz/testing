import { APP_BASE_HREF } from '@angular/common';
import { NgModule } from '@angular/core';

import { ModuleTest } from './module.test';

@NgModule()
class TestModule {}

describe('ModuleTest', () => {
  let test: ModuleTest<TestModule>;

  beforeEach(() => {
    test = new ModuleTest(TestModule);
  });

  it('.testBed have the module imported', () => {
    test.isImported(TestModule);
  });

  it('.testBed have the APP_BASE_HREF provided', () => {
    test.isProvided(APP_BASE_HREF);
  });

  it('.obj is setted with the module', () => {
    expect(test.obj).toEqual(new TestModule());
  });
});
