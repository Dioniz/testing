import { HttpErrorResponse, HttpRequest, HttpResponse } from '@angular/common/http';

/**
 * Test that an HTTP response or request has an specified header.
 *
 * @param obj The HTTP response or request.
 * @param headerName The header name to check.
 * @returns The header value if exist, `null` otherwise.
 */
export function expectHasHeader<V>(
  obj: HttpErrorResponse | HttpResponse<V> | HttpRequest<V>,
  headerName: string
): string | null {
  expect(obj.headers.has(headerName)).toBeTruthy(
    `Expected headers to contain "${headerName}", but are "${obj.headers.keys().toString()}"`
  );

  return obj.headers.get(headerName);
}
