import { HttpResponse } from '@angular/common/http';
import { typeToString } from '@ngxa/utils';

/**
 * Test that the object is an HTTP response and returns it.
 *
 * @param obj The object to test.
 * @returns The object as HTTP response.
 */
export function expectIsHttpResponse<V>(obj: HttpResponse<V> | {} | null): HttpResponse<V> {
  expect(obj instanceof HttpResponse).toBeTruthy(
    `The response must be of type "HttpResponse", but is "${typeToString(obj)}"`
  );

  return obj as HttpResponse<V>;
}
