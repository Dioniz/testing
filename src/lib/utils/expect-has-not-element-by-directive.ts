import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { TestByDirective } from '../typings/test-component.types';

/**
 * Check that do not exist an element with the directive.
 *
 * @param call The test by directive call object.
 * @param element The elemet to test as root.
 */
export function expectHasNotElementByDirective<T>(
  call: TestByDirective<T>,
  element: DebugElement
): void {
  const elements: DebugElement[] = element.queryAll(By.directive(call.directive));

  expect(elements.length === 0).toBeTruthy(
    `Directive "${call.directive.name}" exist in ${element.name}`
  );
}
