import { HttpErrorResponse, HttpRequest, HttpResponse } from '@angular/common/http';

/**
 * Test that an HTTP response or request has not an specified header name.
 *
 * @param obj The HTTP response or request.
 * @param headerName The header name to check.
 */
export function expectHasNotHeader<V>(
  obj: HttpErrorResponse | HttpResponse<V> | HttpRequest<V>,
  headerName: string
): void {
  expect(obj.headers.has(headerName)).toBeFalsy(
    `Expected headers do not to contain "${headerName}", but it has`
  );
}
