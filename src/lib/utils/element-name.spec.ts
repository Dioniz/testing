import { elementName } from './element-name';

describe('elementName', () => {
  it('returns "component template" if null', () => {
    expect(elementName(null)).toEqual('component template');
  });

  it('returns "component template" if undefined', () => {
    expect(elementName(undefined)).toEqual('component template');
  });

  it('returns "component template" if empty string', () => {
    expect(elementName('')).toEqual('component template');
  });

  it('returns name if not empty string', () => {
    expect(elementName('a')).toEqual('a');
  });
});
