import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { TestByDirective } from '../typings/test-component.types';
import { elementName } from './element-name';

/**
 * Check that exist an element with the directive.
 *
 * @param call The test by directive call object.
 * @param element The elemet to test as root.
 * @returns The array of elements that match the directive.
 */
export function expectHasElementByDirective<T>(
  call: TestByDirective<T>,
  element: DebugElement
): DebugElement[] {
  const elements: DebugElement[] = element.queryAll(By.directive(call.directive));

  expect(elements.length > 0).toBeTruthy(
    `Directive "${call.directive.name}" do not exist in ${elementName(element.name)}`
  );

  return elements;
}
