import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { TestBySelector } from '../typings/test-component.types';
import { elementName } from './element-name';

/**
 * Check that exist an element with the selector.
 *
 * @param call The test by selector call object.
 * @param element The elemet to test as root.
 * @returns The array of elements that match the selector.
 */
export function expectHasElementBySelector(
  call: TestBySelector,
  element: DebugElement
): DebugElement[] {
  const elements: DebugElement[] = element.queryAll(By.css(call.selector));

  expect(elements.length > 0).toBeTruthy(
    `Selector "${call.selector}" do not exist in ${elementName(element.name)}`
  );

  return elements;
}
