import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { TestBySelector } from '../typings/test-component.types';

/**
 * Check that do not exist an element with the selector.
 *
 * @param call The test by selector call object.
 * @param element The elemet to test as root.
 */
export function expectHasNotElementBySelector(call: TestBySelector, element: DebugElement): void {
  const elements: DebugElement[] = element.queryAll(By.css(call.selector));

  expect(elements.length === 0).toBeTruthy(
    `Selector "${call.selector}" exist in "${element.name}"`
  );
}
