import { DebugElement, Type } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { defaults, hasProperty, ObjectLiteral, TestBedConfig } from '@ngxa/utils';

import { AngularTestAbstract } from './abstracts/angular-test.abstract';
import { MockActivatedRoute } from './mocks/mock-activated-route.class';
import {
  isTestBySelector,
  TestAttribute,
  TestCssClass,
  TestElement,
  TestElementBy,
  TestElementValue,
  TestEventCall
} from './typings/test-component.types';
import { elementName } from './utils/element-name';
import { expectHasElementByDirective } from './utils/expect-has-element-by-directive';
import { expectHasElementBySelector } from './utils/expect-has-element-by-selector';
import { expectHasNotElementByDirective } from './utils/expect-has-not-element-by-directive';
import { expectHasNotElementBySelector } from './utils/expect-has-not-element-by-selector';

/**
 * Class helper to test Angular components.
 */
export class ComponentTest<T extends object> extends AngularTestAbstract<T> {
  /**
   * Fixture for debugging and testing the component.
   */
  public fixture: ComponentFixture<T>;
  /**
   * The activated route.
   */
  public activatedRoute: MockActivatedRoute;

  /**
   * Creates a class helper to test Angular components.
   * @param clazz The class type to test.
   * @param [config] The custom module configuration needed to test the class.
   * @param args Extra args.
   */
  public constructor(clazz: Type<T>, config: TestBedConfig = new TestBedConfig(), ...args: any[]) {
    super(clazz, config, ...args);
    this.fixture = this.testBed.createComponent(clazz);
    this.obj = this.fixture.componentInstance;
    this.activatedRoute = TestBed.get(ActivatedRoute);
  }

  /**
   * Set the testBed property.
   * @param clazz The class type to test.
   * @param config The custom module configuration needed to test the class.
   * @param args Extra args.
   * @returns The configured TestBed object.
   */
  protected setTestBed(clazz: Type<T>, config: TestBedConfig, ...args: any[]): typeof TestBed {
    config.declarations.push(clazz);
    config.providers.push({ provide: ActivatedRoute, useValue: new MockActivatedRoute() });

    return super.setTestBed(clazz, config);
  }

  /**
   * Check the element exists in the template.
   * @param call The test element by object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   * @returns The array of elements.
   */
  public hasElement<K>(
    call: TestElementBy<K>,
    element: DebugElement = this.fixture.debugElement
  ): DebugElement[] {
    return isTestBySelector(call)
      ? expectHasElementBySelector(call, element)
      : expectHasElementByDirective(call, element);
  }

  /**
   * Check the element do not exists in the template.
   * @param call The test element by object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   */
  public hasNotElement<K>(
    call: TestElementBy<K>,
    element: DebugElement = this.fixture.debugElement
  ): void {
    if (isTestBySelector(call)) {
      expectHasNotElementBySelector(call, element);
    } else {
      expectHasNotElementByDirective(call, element);
    }
  }

  /**
   * Check the element exists in the nth position in the template.
   * @param call The test element object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   * @returns The array of elements.
   */
  public hasNthElement<K>(
    call: TestElement<K>,
    element: DebugElement = this.fixture.debugElement
  ): DebugElement {
    const elements: DebugElement[] = this.hasElement(call, element);
    const nth: number = defaults<number>(call.nth, 1);

    expect(nth).toBeLessThanOrEqual(
      elements.length,
      `Expected an element in the position "${nth}"`
    );

    return elements[nth - 1];
  }

  /**
   * Check the element do not exists in the nth position in the template.
   * @param call The test element object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   */
  public hasNotNthElement<K>(
    call: TestElement<K>,
    element: DebugElement = this.fixture.debugElement
  ): void {
    const nth: number = defaults<number>(call.nth, 1);

    expect(nth).toBeGreaterThan(
      this.hasElement(call, element).length,
      `Expected not an Element in the position "${nth}"`
    );
  }

  /**
   * Check the element has a value.
   * @param call The test element value object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   * @returns The element value.
   */
  public hasElementValue<K>(
    call: TestElementValue<K>,
    element: DebugElement = this.fixture.debugElement
  ): string {
    const value: string = this.hasNthElement(call, element).nativeElement.textContent;

    expect(value).toMatch(call.value);

    return value;
  }

  /**
   * Check the element do not has a value.
   * @param call The test element value object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   */
  public hasNotElementValue<K>(
    call: TestElementValue<K>,
    element: DebugElement = this.fixture.debugElement
  ): void {
    expect(this.hasNthElement(call, element).nativeElement.textContent).not.toMatch(call.value);
  }

  /**
   * Check the element has children.
   * @param call The test element object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   * @returns The array of children elements.
   */
  public hasElementChildren<K>(
    call: TestElement<K>,
    element: DebugElement = this.fixture.debugElement
  ): DebugElement[] {
    const children: DebugElement[] = this.hasNthElement(call, element).children;

    expect(children.length).toBeGreaterThan(
      0,
      `Expected the element "${elementName(element.name)}" to have children`
    );

    return children;
  }

  /**
   * Check the element do not has children.
   * @param call The test element object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   */
  public hasNotElementChildren<K>(
    call: TestElement<K>,
    element: DebugElement = this.fixture.debugElement
  ): void {
    expect(this.hasNthElement(call, element).children.length).toBe(
      0,
      `Expected the element "${elementName(element.name)}" to do not have children`
    );
  }

  /**
   * Check the element has an attribute.
   * @param call The test element attribute object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   * @returns The attribute value as `string`.
   */
  public hasElementAttribute<K>(
    call: TestAttribute<K>,
    element: DebugElement = this.fixture.debugElement
  ): string {
    const attributeObj: ObjectLiteral<string | null> = this.hasNthElement(call, element).attributes;

    expect(hasProperty(attributeObj, call.attribute)).toBeTruthy(
      `Expected the element "${elementName(element.name)}" to have the attribute "${
        call.attribute
      }"`
    );

    return attributeObj[call.attribute] as string;
  }

  /**
   * Check the element do not has an attribute.
   * @param call The test element attribute object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   */
  public hasNotElementAttribute<K>(
    call: TestAttribute<K>,
    element: DebugElement = this.fixture.debugElement
  ): void {
    expect(hasProperty(this.hasNthElement(call, element).attributes, call.attribute)).toBeFalsy(
      `Expected the element "${elementName(element.name)}" to do not have the attribute "${
        call.attribute
      }"`
    );
  }

  /**
   * Check the element has a css class.
   * @param call The test element class object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   * @returns The element value.
   */
  public hasElementClass<K>(
    call: TestCssClass<K>,
    element: DebugElement = this.fixture.debugElement
  ): void {
    expect(
      this.hasNthElement(call, element).nativeElement.classList.contains(call.cssClass)
    ).toBeTruthy(
      `Expected the element "${elementName(element.name)}" to have the class "${call.cssClass}"`
    );
  }

  /**
   * Check the element do not has a css class.
   * @param call The test element class object.
   * @param element The root element to check. Defaults to fixture.debugElement.
   * @returns The element value.
   */
  public hasNotElementClass<K>(
    call: TestCssClass<K>,
    element: DebugElement = this.fixture.debugElement
  ): void {
    expect(
      this.hasNthElement(call, element).nativeElement.classList.contains(call.cssClass)
    ).toBeFalsy(
      `Expected the element "${elementName(element.name)}" to do not have the class "${
        call.cssClass
      }"`
    );
  }

  /**
   * Check that a method is called on event.
   * @param call The event call object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   * @param element The root element to check. Defaults to fixture.debugElement.
   */
  public eventCall<V, O extends object = T>(
    call: TestEventCall<O, V>,
    obj: T | O = this.obj,
    element: DebugElement = this.fixture.debugElement
  ): void {
    this.testEventCall(call, obj, element, (key: keyof T & keyof O) => {
      expect(obj[key]).toHaveBeenCalled();
      if (call.eventObj !== undefined) {
        expect(obj[key]).toHaveBeenCalledWith(call.eventObj);
      }
    });
  }

  /**
   * Check that a method is called on event.
   * @param call The event call object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   * @param element The root element to check. Defaults to fixture.debugElement.
   */
  public eventNotCall<V, O extends object = T>(
    call: TestEventCall<O, V>,
    obj: T | O = this.obj,
    element: DebugElement = this.fixture.debugElement
  ): void {
    this.testEventCall(call, obj, element, (key: keyof T & keyof O) => {
      expect(obj[key]).not.toHaveBeenCalled();
    });
  }

  /**
   * Executes the function on event call.
   * @param call The event call object.
   * @param obj The object to test.
   * @param element The root element to check.
   * @param fn The check function.
   */
  protected testEventCall<V, O extends object>(
    call: TestEventCall<O, V>,
    obj: O,
    element: DebugElement,
    fn: (key: keyof T & keyof O) => void
  ): void {
    const el: DebugElement = this.hasNthElement(call, element);
    const key: keyof T & keyof O = call.key as keyof T & keyof O;

    spyOn(obj, key);
    expect(obj[key]).not.toHaveBeenCalled();
    el.triggerEventHandler(call.eventName, defaults<O | undefined>(call.eventObj, undefined));
    this.fixture.whenStable().then(() => {
      fn(key);
    });
  }
}
