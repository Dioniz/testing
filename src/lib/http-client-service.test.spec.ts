import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
  HttpResponse
} from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Injectable } from '@angular/core';
import { OtherTestClass, TestBedConfig, TestClass } from '@ngxa/utils';
import { Observable } from 'rxjs';

import { HttpClientServiceTest } from './http-client-service.test';

// #region test data

const notFound: string = 'notFoundValue';
const path: string = '/basePath';
const basePath2: string = '/basePath2';

const body: {} = new TestClass();
const error: {} = new TestClass();
const errorEvent: ErrorEvent = new ErrorEvent('error');
const notValue: {} = { id: 0 };
const status: number = 200;
const errorStatus: number = 204;
const statusError: number = 400;
const notStatusError: number = 404;
const statusText: string = 'Ok';
const notStatusTextOk: string = 'No content';
const statusTextError: string = 'Bad Request';
const notStatusTextError: string = 'Not Found';
const httpHeaders: HttpHeaders = new HttpHeaders({ header1: 'header_1' });
const response: HttpResponse<any> = new HttpResponse({
  body,
  status,
  statusText,
  headers: httpHeaders
});
const responseError: HttpErrorResponse = new HttpErrorResponse({
  error,
  status: statusError,
  statusText: statusTextError,
  headers: httpHeaders
});
const responseErrorEvent: HttpErrorResponse = new HttpErrorResponse({
  error: errorEvent,
  status: statusError,
  statusText: statusTextError,
  headers: httpHeaders
});
const httpParams: HttpParams = new HttpParams().set('param1', 'param_1');

@Injectable()
class TestHttpService {
  public subpath: string = path;

  public constructor(protected http: HttpClient) {}

  public setPath(subpath?: string): void {
    this.subpath = subpath !== undefined ? subpath : basePath2;
  }

  public responseMethod(id?: number): Observable<HttpResponse<any>> {
    return id !== undefined
      ? this.http.get(`${this.subpath}/${id}`, { observe: 'response' })
      : this.http.get(this.subpath, { observe: 'response' });
  }

  public getMethod(id?: number): Observable<any> {
    return id !== undefined
      ? this.http.get(`${this.subpath}/${id}`, { headers: httpHeaders })
      : this.http.get(this.subpath, { headers: httpHeaders });
  }

  public credentialsMethod(id?: number): Observable<any> {
    return id !== undefined
      ? this.http.get(`${this.subpath}/${id}`, { withCredentials: true })
      : this.http.get(this.subpath, { withCredentials: true });
  }

  public responseTypeMethod(id?: number): Observable<any> {
    return id !== undefined
      ? this.http.get(`${this.subpath}/${id}`, { responseType: 'text' })
      : this.http.get(this.subpath, { responseType: 'text' });
  }

  public progressMethod(id?: number): Observable<any> {
    return id !== undefined
      ? this.http.get(`${this.subpath}/${id}`, { reportProgress: true })
      : this.http.get(this.subpath, { reportProgress: true });
  }

  public paramsMethod(id?: number): Observable<any> {
    return id !== undefined
      ? this.http.get(`${this.subpath}/${id}`, { params: httpParams })
      : this.http.get(this.subpath, { params: httpParams });
  }

  public headMethod(id?: number): Observable<any> {
    return id !== undefined
      ? this.http.head(`${this.subpath}/${id}`)
      : this.http.head(this.subpath);
  }

  public postMethod(id?: number): Observable<any> {
    return id !== undefined
      ? this.http.post(`${this.subpath}/${id}`, body)
      : this.http.post(this.subpath, body);
  }

  public putMethod(id?: number): Observable<any> {
    return id !== undefined
      ? this.http.put(`${this.subpath}/${id}`, body)
      : this.http.put(this.subpath, body);
  }

  public patchMethod(id?: number): Observable<any> {
    return id !== undefined
      ? this.http.patch(`${this.subpath}/${id}`, body)
      : this.http.patch(this.subpath, body);
  }

  public deleteMethod(id?: number): Observable<any> {
    return id !== undefined
      ? this.http.delete(`${this.subpath}/${id}`)
      : this.http.delete(this.subpath);
  }
}

// tslint:disable-next-line:max-classes-per-file
@Injectable()
class TestHttpService2 extends TestHttpService {
  public constructor(protected http: HttpClient) {
    super(http);
  }
}

// #endregion

describe('HttpClientServiceTest', () => {
  let test: HttpClientServiceTest<TestHttpService>;
  let config: TestBedConfig;
  let obj: TestHttpService2;

  beforeEach(() => {
    config = new TestBedConfig();
    config.providers.push(TestHttpService2);
    test = new HttpClientServiceTest(TestHttpService, config);
    obj = test.testBed.get(TestHttpService2);
  });

  it('.testBed have HttpClientTestingModule as import', () => {
    expect(test.testBed.get(HttpClientTestingModule, notFound)).not.toEqual(notFound);
  });

  it('.httpMock is setted with the HttpTestingController', () => {
    expect(test.httpMock).toBeDefined();
    expect(test.httpMock).toEqual(test.testBed.get(HttpTestingController));
  });

  // #region .sendsRequest(call, obj?)

  it('.sendsRequest({key, path}) test method sends request', () => {
    test.sendsRequest({ key: 'getMethod', path });
  });

  it('.sendsRequest({key, path}, obj) test method sends request with object', () => {
    test.sendsRequest({ key: 'getMethod', path }, obj);
  });

  // #endregion

  // #region .sendsRequestMethod(call, obj?)

  it('.sendsRequestMethod({key, method, path}) test method sends request method GET', () => {
    test.sendsRequestMethod({ key: 'getMethod', method: 'GET', path });
  });

  it('.sendsRequestMethod({key, method, path}) test method sends request method HEAD', () => {
    test.sendsRequestMethod({ key: 'headMethod', method: 'HEAD', path });
  });

  it('.sendsRequestMethod({key, method, path}) test method sends request method POST', () => {
    test.sendsRequestMethod({ key: 'postMethod', method: 'POST', path });
  });

  it('.sendsRequestMethod({key, method, path}) test method sends request method PUT', () => {
    test.sendsRequestMethod({ key: 'putMethod', method: 'PUT', path });
  });

  it('.sendsRequestMethod({key, method, path}) test method sends request method PATCH', () => {
    test.sendsRequestMethod({ key: 'patchMethod', method: 'PATCH', path });
  });

  it('.sendsRequestMethod({key, method, path}) test method sends request method DELETE', () => {
    test.sendsRequestMethod({ key: 'deleteMethod', method: 'DELETE', path });
  });

  it('.sendsRequestMethod({key, method, path}, obj) test method sends request method GET with object', () => {
    test.sendsRequestMethod({ key: 'getMethod', method: 'GET', path }, obj);
  });

  it('.sendsRequestMethod({key, method, path}, obj) test method sends request method HEAD with object', () => {
    test.sendsRequestMethod({ key: 'headMethod', method: 'HEAD', path }, obj);
  });

  it('.sendsRequestMethod({key, method, path}, obj) test method sends request method POST with object', () => {
    test.sendsRequestMethod({ key: 'postMethod', method: 'POST', path }, obj);
  });

  it('.sendsRequestMethod({key, method, path}, obj) test method sends request method PUT with object', () => {
    test.sendsRequestMethod({ key: 'putMethod', method: 'PUT', path }, obj);
  });

  it('.sendsRequestMethod({key, method, path}, obj) test method sends request method PATCH with object', () => {
    test.sendsRequestMethod({ key: 'patchMethod', method: 'PATCH', path }, obj);
  });

  it('.sendsRequestMethod({key, method, path}, obj) test method sends request method DELETE with object', () => {
    test.sendsRequestMethod({ key: 'deleteMethod', method: 'DELETE', path }, obj);
  });

  // #endregion

  // #region .sendsNotRequestMethod(call, obj?)

  it('.sendsNotRequestMethod({key, method, path}) test method do not sends request method', () => {
    test.sendsNotRequestMethod({ key: 'getMethod', method: 'POST', path });
  });

  it('.sendsNotRequestMethod({key, method, path}, obj) test method do not sends request method with object', () => {
    test.sendsNotRequestMethod({ key: 'getMethod', method: 'POST', path }, obj);
  });

  // #endregion

  // #region .sendsRequestBody(call, obj?)

  it('.sendsRequestBody({key, body, path}) test method sends request null body', () => {
    test.sendsRequestBody({ key: 'getMethod', body: null, path });
    test.sendsRequestBody({ key: 'headMethod', body: null, path });
    test.sendsRequestBody({ key: 'deleteMethod', body: null, path });
  });

  it('.sendsRequestBody({key, body, path}) test method sends request body', () => {
    test.sendsRequestBody({ key: 'postMethod', body, path });
    test.sendsRequestBody({ key: 'putMethod', body, path });
    test.sendsRequestBody({ key: 'patchMethod', body, path });
  });

  it('.sendsRequestBody({key, body, path}, obj) test method sends request null body with object', () => {
    test.sendsRequestBody({ key: 'getMethod', body: null, path }, obj);
    test.sendsRequestBody({ key: 'headMethod', body: null, path }, obj);
    test.sendsRequestBody({ key: 'deleteMethod', body: null, path }, obj);
  });

  it('.sendsRequestBody({key, body, path}, obj) test method sends request body with object', () => {
    test.sendsRequestBody({ key: 'postMethod', body, path }, obj);
    test.sendsRequestBody({ key: 'putMethod', body, path }, obj);
    test.sendsRequestBody({ key: 'patchMethod', body, path }, obj);
  });

  // #endregion

  // #region .sendsNotRequestBody(call, obj?)

  it('.sendsNotRequestBody({key, body, path}) test method sends request body', () => {
    test.sendsNotRequestBody({ key: 'getMethod', body: notValue, path });
    test.sendsNotRequestBody({ key: 'postMethod', body: notValue, path });
  });

  it('.sendsNotRequestBody({key, body, path}, obj) test method sends request null body with object', () => {
    test.sendsNotRequestBody({ key: 'getMethod', body: notValue, path }, obj);
    test.sendsNotRequestBody({ key: 'postMethod', body: notValue, path }, obj);
  });

  // #endregion

  // #region .sendsRequestBodyTypeOf(call, obj?)

  it('.sendsRequestBodyTypeOf({key, typeOf, path}) test method sends request body type', () => {
    test.sendsRequestBodyType({ key: 'postMethod', typeOf: 'object', path });
  });

  it('.sendsRequestBodyTypeOf({key, typeOf, path}, obj) test method sends request body type with json', () => {
    test.sendsRequestBodyType({ key: 'postMethod', typeOf: 'object', path }, obj);
  });

  // #endregion

  // #region .sendsNotRequestBodyTypeOf(call, obj?)

  it('.sendsNotRequestBodyTypeOf({key, typeOf, path}) test method do not sends request body type', () => {
    test.sendsNotRequestBodyType({ key: 'postMethod', typeOf: 'string', path });
  });

  it('.sendsNotRequestBodyTypeOf({key, typeOf, path}, obj) test method do not sends request body type with json', () => {
    test.sendsNotRequestBodyType({ key: 'postMethod', typeOf: 'string', path }, obj);
  });

  // #endregion

  // #region .sendsRequestBodyInstanceOf(call, obj?)

  it('.sendsRequestBodyInstanceOf({key, instanceOf, path}) test method sends request body instance type', () => {
    test.sendsRequestBodyInstance({ key: 'postMethod', instanceOf: TestClass, path });
  });

  it('.sendsRequestBodyInstanceOf({key, instanceOf, path}, obj) test method sends request body instance type with json', () => {
    test.sendsRequestBodyInstance({ key: 'postMethod', instanceOf: TestClass, path }, obj);
  });

  // #endregion

  // #region .sendsNotRequestBodyInstanceOf(call, obj?)

  it('.sendsNotRequestBodyInstanceOf({key, instanceOf, path}) test method do not sends request body instance type', () => {
    test.sendsNotRequestBodyInstance({ key: 'postMethod', instanceOf: OtherTestClass, path });
  });

  it('.sendsNotRequestBodyInstanceOf({key, instanceOf, path}, obj) test method do not sends request body instance type with json', () => {
    test.sendsNotRequestBodyInstance({ key: 'postMethod', instanceOf: OtherTestClass, path }, obj);
  });

  // #endregion

  // #region .sendsRequestHeader(call, obj?)

  it('.sendsRequestHeader({key, header, path}) test method sends request header', () => {
    test.sendsRequestHeader({ key: 'getMethod', header: 'header1', path });
  });

  it('.sendsRequestHeader({key, header, path}, obj) test method sends request header with object', () => {
    test.sendsRequestHeader({ key: 'getMethod', header: 'header1', path }, obj);
  });

  // #endregion

  // #region .sendsNotRequestHeader(call, obj?)

  it('.sendsNotRequestHeader({key, header, path}) test method do not sends request header', () => {
    test.sendsNotRequestHeader({ key: 'getMethod', header: 'header2', path });
  });

  it('.sendsNotRequestHeader({key, header, path}, obj) test method do not sends request header with object', () => {
    test.sendsNotRequestHeader({ key: 'getMethod', header: 'header2', path }, obj);
  });

  // #endregion

  // #region .sendsRequestHeaderValue(call, obj?)

  it('.sendsRequestHeaderValue({key, header, value, path}) test method sends request header value', () => {
    test.sendsRequestHeaderValue({ key: 'getMethod', header: 'header1', value: 'header_1', path });
  });

  it('.sendsRequestHeaderValue({key, header, value, path}) test method sends request header regexp value', () => {
    test.sendsRequestHeaderValue({ key: 'getMethod', header: 'header1', value: '^h', path });
  });

  it('.sendsRequestHeaderValue({key, header, value, path}, obj) test method sends request header value with object', () => {
    test.sendsRequestHeaderValue(
      { key: 'getMethod', header: 'header1', value: 'header_1', path },
      obj
    );
  });

  // #endregion

  // #region .sendsNotRequestHeaderValue(call, obj?)

  it('.sendsNotRequestHeaderValue({key, header, value, path}) test method do not sends request header value', () => {
    test.sendsNotRequestHeaderValue({
      key: 'getMethod',
      header: 'header1',
      value: 'header1',
      path
    });
  });

  it('.sendsNotRequestHeaderValue({key, header, value, path}) test method do not sends request header regexp value', () => {
    test.sendsNotRequestHeaderValue({
      key: 'getMethod',
      header: 'header1',
      value: '^a',
      path
    });
  });

  it('.sendsNotRequestHeaderValue({key, header, value, path}, obj) test method do not sends request header value with object', () => {
    test.sendsNotRequestHeaderValue(
      { key: 'getMethod', header: 'header1', value: 'header1', path },
      obj
    );
  });

  // #endregion

  // #region .sendsRequestParam(call, obj?)

  it('.sendsRequestParam({key, param, path}) test method sends request with param', () => {
    test.sendsRequestParam({ key: 'paramsMethod', param: 'param1', path });
  });

  it('.sendsRequestParam({key, param, path}, obj) test method sends request with param and object', () => {
    test.sendsRequestParam({ key: 'paramsMethod', param: 'param1', path }, obj);
  });

  // #endregion

  // #region .sendsNotRequestParam(call, obj?)

  it('.sendsNotRequestParam({key, param, path}) test method do not sends request with param', () => {
    test.sendsNotRequestParam({ key: 'paramsMethod', param: 'param2', path });
  });

  it('.sendsNotRequestParam({key, param, path}, obj) test method do not sends request with param and object', () => {
    test.sendsNotRequestParam({ key: 'paramsMethod', param: 'param2', path }, obj);
  });

  // #endregion

  // #region .sendsRequestParamValue(call, obj?)

  it('.sendsRequestParamValue({key, param, value, path}) test method sends request param value', () => {
    test.sendsRequestParamValue({ key: 'paramsMethod', param: 'param1', value: 'param_1', path });
  });

  it('.sendsRequestParamValue({key, param, value, path}) test method sends request param regexp value', () => {
    test.sendsRequestParamValue({ key: 'paramsMethod', param: 'param1', value: '^p', path });
  });

  it('.sendsRequestParamValue({key, param, value, path}, obj) test method sends request param value with object', () => {
    test.sendsRequestParamValue(
      { key: 'paramsMethod', param: 'param1', value: 'param_1', path },
      obj
    );
  });

  // #endregion

  // #region .sendsNotRequestParamValue(call, obj?)

  it('.sendsNotRequestParamValue({key, param, value, path}) test method sends request param value', () => {
    test.sendsNotRequestParamValue({ key: 'paramsMethod', param: 'param1', value: 'param1', path });
  });

  it('.sendsNotRequestParamValue({key, param, value, path}) test method sends request param regexp value', () => {
    test.sendsNotRequestParamValue({ key: 'paramsMethod', param: 'param1', value: '^a', path });
  });

  it('.sendsNotRequestParamValue({key, param, value, path}, obj) test method sends request param value with object', () => {
    test.sendsNotRequestParamValue(
      { key: 'paramsMethod', param: 'param1', value: 'param1', path },
      obj
    );
  });

  // #endregion

  // #region .sendsRequestWithCredentials(call, obj?)

  it('.sendsRequestWithCredentials({key, path}) test method sends request with credentials', () => {
    test.sendsRequestWithCredentials({ key: 'credentialsMethod', path });
  });

  it('.sendsRequestWithCredentials({key, path}, obj) test method sends request with credentials and object', () => {
    test.sendsRequestWithCredentials({ key: 'credentialsMethod', path }, obj);
  });

  // #endregion

  // #region .sendsNotRequestWithCredentials(call, obj?)

  it('.sendsNotRequestWithCredentials({key, path}) test method sends request with credentials', () => {
    test.sendsNotRequestWithCredentials({ key: 'getMethod', path });
  });

  it('.sendsNotRequestWithCredentials({key, path}, obj) test method sends request with credentials and object', () => {
    test.sendsNotRequestWithCredentials({ key: 'getMethod', path }, obj);
  });

  // #endregion

  // #region .sendsRequestReportProgress(call, obj?)

  it('.sendsRequestReportProgress({key, path}) test method sends request with report progress', () => {
    test.sendsRequestReportProgress({ key: 'progressMethod', path });
  });

  it('.sendsRequestReportProgress({key, path}, obj) test method sends request with report progress and object', () => {
    test.sendsRequestReportProgress({ key: 'progressMethod', path }, obj);
  });

  // #endregion

  // #region .sendsNotRequestReportProgress(call, obj?)

  it('.sendsNotRequestReportProgress({key, path}) test method sends request with report progress', () => {
    test.sendsNotRequestReportProgress({ key: 'getMethod', path });
  });

  it('.sendsNotRequestReportProgress({key, path}, obj) test method sends request with report progress and object', () => {
    test.sendsNotRequestReportProgress({ key: 'getMethod', path }, obj);
  });

  // #endregion

  // #region .sendsRequestResponseType(call, obj?)

  it('.sendsRequestResponseType({key, path}) test method sends request with text response type', () => {
    test.sendsRequestResponseType({ key: 'responseTypeMethod', responseType: 'text', path });
  });

  it('.sendsRequestResponseType({key, path}) test method sends request with json response type', () => {
    test.sendsRequestResponseType({ key: 'getMethod', responseType: 'json', path });
  });

  it('.sendsRequestResponseType({key, path}, obj) test method sends request with text response type and object', () => {
    test.sendsRequestResponseType({ key: 'responseTypeMethod', responseType: 'text', path }, obj);
  });

  it('.sendsRequestResponseType({key, path}, obj) test method sends request with json response type and object', () => {
    test.sendsRequestResponseType({ key: 'getMethod', responseType: 'json', path }, obj);
  });

  // #endregion

  // #region .sendsNotRequestResponseType(call, obj?)

  it('.sendsNotRequestResponseType({key, path}) test method do not sends request with text response type', () => {
    test.sendsNotRequestResponseType({ key: 'responseTypeMethod', responseType: 'json', path });
  });

  it('.sendsNotRequestResponseType({key, path}, obj) test method do not sends request with text response type and object', () => {
    test.sendsNotRequestResponseType(
      { key: 'responseTypeMethod', responseType: 'json', path },
      obj
    );
  });

  // #endregion

  // #region .returnsResponse(call, obj?)

  it('.returnsResponse({key, path, response}) test method returns response', () => {
    test.returnsResponse({ key: 'getMethod', path, response });
  });

  it('.returnsResponse({key, path, response}, obj) test method returns response with object', () => {
    test.returnsResponse({ key: 'getMethod', path, response }, obj);
  });

  // #endregion

  // #region .returnsResponseValue(call, obj?)

  it('.returnsResponseValue({key, body, path, response}) test method returns response body', () => {
    test.returnsResponseValue({ key: 'getMethod', value: body, path, response });
  });

  it('.returnsResponseValue({key, body, path, response}, obj) test method returns response body with object', () => {
    test.returnsResponseValue({ key: 'getMethod', value: body, path, response }, obj);
  });

  // #endregion

  // #region .returnsNotResponseValue(call, obj?)

  it('.returnsResponseValue({key, body, path, response}) test method returns response body', () => {
    test.returnsNotResponseValue({ key: 'getMethod', value: notValue, path, response });
  });

  it('.returnsResponseValue({key, body, path, response}, obj) test method returns response body with object', () => {
    test.returnsNotResponseValue({ key: 'getMethod', value: notValue, path, response }, obj);
  });

  // #endregion

  // #region .returnsResponseValueType(call, obj?)

  it('.returnsResponseValueType({key, typeOf, path, response}) test method returns response body type', () => {
    test.returnsResponseValueType({ key: 'getMethod', typeOf: 'object', path, response });
  });

  it('.returnsResponseValueType({key, typeOf, path, response}, obj) test method returns response body type with json', () => {
    test.returnsResponseValueType({ key: 'getMethod', typeOf: 'object', path, response }, obj);
  });

  // #endregion

  // #region .returnsNotResponseValueType(call, obj?)

  it('.returnsNotResponseValueType({key, typeOf, path, response}) test method do not returns response body type', () => {
    test.returnsNotResponseValueType({ key: 'getMethod', typeOf: 'string', path, response });
  });

  it('.returnsNotResponseValueType({key, typeOf, path, response}, obj) test method do not returns response body type with object', () => {
    test.returnsNotResponseValueType({ key: 'getMethod', typeOf: 'string', path, response }, obj);
  });

  // #endregion

  // #region .returnsResponseValueInstance(call, obj?)

  it('.returnsResponseValueInstance({key, instanceOf, path, response}) test method returns response body instance type', () => {
    test.returnsResponseValueInstance({
      key: 'getMethod',
      instanceOf: TestClass,
      path,
      response
    });
  });

  it('.returnsResponseValueInstance({key, instanceOf, path, response}, obj) test method returns response body instance type with object', () => {
    test.returnsResponseValueInstance(
      { key: 'getMethod', instanceOf: TestClass, path, response },
      obj
    );
  });

  // #endregion

  // #region .returnsNotResponseValueInstance(call, obj?)

  it('.returnsNotResponseValueInstance({key, instanceOf, path, response}) test method do not returns response body instance type', () => {
    test.returnsNotResponseValueInstance({
      key: 'getMethod',
      instanceOf: OtherTestClass,
      path,
      response
    });
  });

  it('.returnsNotResponseValueInstance({key, instanceOf, path, response}, obj) test method do not returns response body instance type with object', () => {
    test.returnsNotResponseValueInstance(
      { key: 'getMethod', instanceOf: OtherTestClass, path, response },
      obj
    );
  });

  // #endregion

  // #region .returnsResponseBody(call, obj?)

  it('.returnsResponseBody({key, body, path, response}) test method returns response body', () => {
    test.returnsResponseBody({ key: 'responseMethod', body, path, response });
  });

  it('.returnsResponseBody({key, body, path, response}, obj) test method returns response body with object', () => {
    test.returnsResponseBody({ key: 'responseMethod', body, path, response }, obj);
  });

  // #endregion

  // #region .returnsNotResponseBody(call, obj?)

  it('.returnsNotResponseBody({key, body, path, response}) test method do not returns response body', () => {
    test.returnsNotResponseBody({ key: 'responseMethod', body: notValue, path, response });
  });

  it('.returnsNotResponseBody({key, body, path, response}, obj) test method do not returns response body with object', () => {
    test.returnsNotResponseBody({ key: 'responseMethod', body: notValue, path, response }, obj);
  });

  // #endregion

  // #region .returnsResponseBodyTypeOf(call, obj?)

  it('.returnsResponseBodyTypeOf({key, typeOf, path, response}) test method returns response body type', () => {
    test.returnsResponseBodyType({ key: 'responseMethod', typeOf: 'object', path, response });
  });

  it('.returnsResponseBodyTypeOf({key, typeOf, path, response}, obj) test method returns response body type with json', () => {
    test.returnsResponseBodyType({ key: 'responseMethod', typeOf: 'object', path, response }, obj);
  });

  // #endregion

  // #region .returnsNotResponseBodyTypeOf(call, obj?)

  it('.returnsNotResponseBodyTypeOf({key, typeOf, path, response}) test method do not returns response body type', () => {
    test.returnsNotResponseBodyType({ key: 'responseMethod', typeOf: 'string', path, response });
  });

  it('.returnsNotResponseBodyTypeOf({key, typeOf, path, response}, obj) test method do not returns response body type with object', () => {
    test.returnsNotResponseBodyType(
      { key: 'responseMethod', typeOf: 'string', path, response },
      obj
    );
  });

  // #endregion

  // #region .returnsResponseBodyInstanceOf(call, obj?)

  it('.returnsResponseBodyInstanceOf({key, instanceOf, path, response}) test method returns response body instance type', () => {
    test.returnsResponseBodyInstance({
      key: 'responseMethod',
      instanceOf: TestClass,
      path,
      response
    });
  });

  it('.returnsResponseBodyInstanceOf({key, instanceOf, path, response}, obj) test method returns response body instance type with object', () => {
    test.returnsResponseBodyInstance(
      { key: 'responseMethod', instanceOf: TestClass, path, response },
      obj
    );
  });

  // #endregion

  // #region .returnsNotResponseBodyInstanceOf(call, obj?)

  it('.returnsNotResponseBodyInstanceOf({key, instanceOf, path, response}) test method do not returns response body instance type', () => {
    test.returnsNotResponseBodyInstance({
      key: 'responseMethod',
      instanceOf: OtherTestClass,
      path,
      response
    });
  });

  it('.returnsNotResponseBodyInstanceOf({key, instanceOf, path, response}, obj) test method do not returns response body instance type with object', () => {
    test.returnsNotResponseBodyInstance(
      { key: 'responseMethod', instanceOf: OtherTestClass, path, response },
      obj
    );
  });

  // #endregion

  // #region .returnsResponseStatus(call, obj?)

  it('.returnsResponseStatus({key, status, path, response}) test method returns response status', () => {
    test.returnsResponseStatus({ key: 'responseMethod', status, path, response });
  });

  it('.returnsResponseStatus({key, status, path, response}, obj) test method returns response status with object', () => {
    test.returnsResponseStatus({ key: 'responseMethod', status, path, response }, obj);
  });

  // #endregion

  // #region .returnsNotResponseStatus(call, obj?)

  it('.returnsNotResponseStatus({key, status, path, response}) test method do not returns response status', () => {
    test.returnsNotResponseStatus({ key: 'responseMethod', status: errorStatus, path, response });
  });

  it('.returnsNotResponseStatus({key, status, path, response}, obj) test method do not returns response status with object', () => {
    test.returnsNotResponseStatus(
      { key: 'responseMethod', status: errorStatus, path, response },
      obj
    );
  });

  // #endregion

  // #region .returnsResponseStatusText(call, obj?)

  it('.returnsResponseStatusText({key, statusText, path, response}) test method returns response statusText', () => {
    test.returnsResponseStatusText({ key: 'responseMethod', statusText, path, response });
  });

  it('.returnsResponseStatusText({key, statusText, path, response}, obj) test method returns response statusText with object', () => {
    test.returnsResponseStatusText({ key: 'responseMethod', statusText, path, response }, obj);
  });

  // #endregion

  // #region .returnsNotResponseStatusText(call, obj?)

  it('.returnsNotResponseStatusText({key, statusText, path, response}) test method do not returns response statusText', () => {
    test.returnsNotResponseStatusText({
      key: 'responseMethod',
      statusText: notStatusTextOk,
      path,
      response
    });
  });

  it('.returnsNotResponseStatusText({key, statusText, path, response}, obj) test method do not returns response statusText with object', () => {
    test.returnsNotResponseStatusText(
      { key: 'responseMethod', statusText: notStatusTextOk, path, response },
      obj
    );
  });

  // #endregion

  // #region .returnsResponseHeader(call, obj?)

  it('.returnsResponseHeader({key, header, path, response}) test method returns response header', () => {
    test.returnsResponseHeader({ key: 'responseMethod', header: 'header1', path, response });
  });

  it('.returnsResponseHeader({key, header, path, response}, obj) test method returns response header with object', () => {
    test.returnsResponseHeader({ key: 'responseMethod', header: 'header1', path, response }, obj);
  });

  // #endregion

  // #region .returnsNotResponseHeader(call, obj?)

  it('.returnsNotResponseHeader({key, header, path, response}) test method do not returns response header', () => {
    test.returnsNotResponseHeader({ key: 'responseMethod', header: 'header_1', path, response });
  });

  it('.returnsNotResponseHeader({key, header, path, response}, obj) test method do not returns response header with object', () => {
    test.returnsNotResponseHeader(
      { key: 'responseMethod', header: 'header_1', path, response },
      obj
    );
  });

  // #endregion

  // #region .returnsResponseHeaderValue(call, obj?)

  it('.returnsResponseHeaderValue({key, header, value, path, response}) test method returns response header value', () => {
    test.returnsResponseHeaderValue({
      key: 'responseMethod',
      header: 'header1',
      value: 'header_1',
      path,
      response
    });
  });

  it('.returnsResponseHeaderValue({key, header, value, path, response}) test method returns response header regexp value', () => {
    test.returnsResponseHeaderValue({
      key: 'responseMethod',
      header: 'header1',
      value: '^h',
      path,
      response
    });
  });

  it('.returnsResponseHeaderValue({key, header, value, path, response}, obj) test method returns response header value with object', () => {
    test.returnsResponseHeaderValue(
      { key: 'responseMethod', header: 'header1', value: 'header_1', path, response },
      obj
    );
  });

  // #endregion

  // #region .returnsNotResponseHeaderValue(call, obj?)

  it('.returnsNotResponseHeaderValue({key, header, value, path, response}) test method do not returns response header value', () => {
    test.returnsNotResponseHeaderValue({
      key: 'responseMethod',
      header: 'header1',
      value: 'header1',
      path,
      response
    });
  });

  it('.returnsNotResponseHeaderValue({key, header, value, path, response}) test method do not returns response header regexp value', () => {
    test.returnsNotResponseHeaderValue({
      key: 'responseMethod',
      header: 'header1',
      value: '^a',
      path,
      response
    });
  });

  it('.returnsNotResponseHeaderValue({key, header, value, path, response}, obj) test method do not returns response header with object', () => {
    test.returnsNotResponseHeaderValue(
      { key: 'responseMethod', header: 'header1', value: 'header1', path, response },
      obj
    );
  });

  // #endregion

  // #region .returnsErrorResponse(call, obj?)

  it('.returnsErrorResponse({key, path, response}) test method returns error response', () => {
    test.returnsErrorResponse({ key: 'getMethod', path, response: responseError });
  });

  it('.returnsErrorResponse({key, path, response}, obj) test method returns error response with object', () => {
    test.returnsErrorResponse({ key: 'getMethod', path, response: responseError }, obj);
  });

  // #endregion

  // #region .returnsErrorResponseError(call, obj?)

  it('.returnsErrorResponseError({key, error, path, response}) test method returns error response error', () => {
    test.returnsErrorResponseError({ key: 'getMethod', error, path, response: responseError });
  });

  it('.returnsErrorResponseError({key, error, path, response}, obj) test method returns error response error with object', () => {
    test.returnsErrorResponseError({ key: 'getMethod', error, path, response: responseError }, obj);
  });

  // #endregion

  // #region .returnsNotErrorResponseError(call, obj?)

  it('.returnsNotErrorResponseError({key, error, path, response}) test method do not returns error response error', () => {
    test.returnsNotErrorResponseError({
      key: 'getMethod',
      error: notValue,
      path,
      response: responseError
    });
  });

  it('.returnsNotErrorResponseError({key, error, path, response}, obj) test method do not returns error response error with object', () => {
    test.returnsNotErrorResponseError(
      { key: 'getMethod', error: notValue, path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsErrorResponseErrorTypeOf(call, obj?)

  it('.returnsErrorResponseErrorTypeOf({key, typeOf, path, response}) test method returns error response error type', () => {
    test.returnsErrorResponseErrorType({
      key: 'getMethod',
      typeOf: 'object',
      path,
      response: responseError
    });
  });

  it('.returnsErrorResponseErrorTypeOf({key, typeOf, path, response}, obj) test method returns error response error type with object', () => {
    test.returnsErrorResponseErrorType(
      { key: 'getMethod', typeOf: 'object', path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsNotErrorResponseErrorTypeOf(call, obj?)

  it('.returnsNotErrorResponseErrorTypeOf({key, typeOf, path, response}) test method do not returns error response error type', () => {
    test.returnsNotErrorResponseErrorType({
      key: 'getMethod',
      typeOf: 'string',
      path,
      response: responseError
    });
  });

  it('.returnsNotErrorResponseErrorTypeOf({key, typeOf, path, response}, obj) test method do not returns error response error type with object', () => {
    test.returnsNotErrorResponseErrorType(
      { key: 'getMethod', typeOf: 'string', path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsErrorResponseErrorInstanceOf(call, obj?)

  it('.returnsErrorResponseErrorInstanceOf({key, instanceOf, path, response}) test method returns error response error instance type', () => {
    test.returnsErrorResponseErrorInstance({
      key: 'getMethod',
      instanceOf: TestClass,
      path,
      response: responseError
    });
  });

  it('.returnsErrorResponseErrorInstanceOf({key, instanceOf, path, response}, obj) test method returns error response error instance type with object', () => {
    test.returnsErrorResponseErrorInstance(
      { key: 'getMethod', instanceOf: TestClass, path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsNotErrorResponseErrorInstanceOf(call, obj?)

  it('.returnsNotErrorResponseErrorInstanceOf({key, instanceOf, path, response}) test method do not returns error response error instance type', () => {
    test.returnsNotErrorResponseErrorInstance({
      key: 'getMethod',
      instanceOf: OtherTestClass,
      path,
      response: responseError
    });
  });

  it('.returnsNotErrorResponseErrorInstanceOf({key, instanceOf, path, response}, obj) test method do not returns error response error instance type with object', () => {
    test.returnsNotErrorResponseErrorInstance(
      { key: 'getMethod', instanceOf: OtherTestClass, path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsErrorResponseStatus(call, obj?)

  it('.returnsErrorResponseStatus({key, status, path, response}) test method returns error response status', () => {
    test.returnsErrorResponseStatus({
      key: 'responseMethod',
      status: statusError,
      path,
      response: responseError
    });
  });

  it('.returnsErrorResponseStatus({key, status, path, response}, obj) test method returns error response status with object', () => {
    test.returnsErrorResponseStatus(
      { key: 'responseMethod', status: statusError, path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsNotErrorResponseStatus(call, obj?)

  it('.returnsNotErrorResponseStatus({key, status, path, response}) test method do not returns error response status', () => {
    test.returnsNotErrorResponseStatus({
      key: 'responseMethod',
      status: notStatusError,
      path,
      response: responseError
    });
  });

  it('.returnsNotErrorResponseStatus({key, status, path, response}, obj) test method do not returns error response status with object', () => {
    test.returnsNotErrorResponseStatus(
      { key: 'responseMethod', status: notStatusError, path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsErrorResponseStatusText(call, obj?)

  it('.returnsErrorResponseStatusText({key, statusText, path, response}) test method returns error response statusText', () => {
    test.returnsErrorResponseStatusText({
      key: 'responseMethod',
      statusText: statusTextError,
      path,
      response: responseError
    });
  });

  it('.returnsErrorResponseStatusText({key, statusText, path, response}, obj) test method returns error response statusText with object', () => {
    test.returnsErrorResponseStatusText(
      { key: 'responseMethod', statusText: statusTextError, path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsNotErrorResponseStatusText(call, obj?)

  it('.returnsNotErrorResponseStatusText({key, statusText, path, response}) test method do not returns error response statusText', () => {
    test.returnsNotErrorResponseStatusText({
      key: 'responseMethod',
      statusText: notStatusTextError,
      path,
      response: responseError
    });
  });

  it('.returnsNotErrorResponseStatusText({key, statusText, path, response}, obj) test method do not returns error response statusText with object', () => {
    test.returnsNotErrorResponseStatusText(
      { key: 'responseMethod', statusText: notStatusTextError, path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsErrorResponseHeader(call, obj?)

  it('.returnsErrorResponseHeader({key, header, path, response}) test method returns error response header', () => {
    test.returnsErrorResponseHeader({
      key: 'responseMethod',
      header: 'header1',
      path,
      response: responseError
    });
  });

  it('.returnsErrorResponseHeader({key, header, path, response}, obj) test method returns error response header with object', () => {
    test.returnsErrorResponseHeader(
      { key: 'responseMethod', header: 'header1', path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsNotErrorResponseHeader(call, obj?)

  it('.returnsNotErrorResponseHeader({key, header, path, response}) test method do not returns error response header', () => {
    test.returnsNotErrorResponseHeader({
      key: 'responseMethod',
      header: 'header_1',
      path,
      response: responseError
    });
  });

  it('.returnsNotErrorResponseHeader({key, header, path, response}, obj) test method do not returns error response header with object', () => {
    test.returnsNotErrorResponseHeader(
      { key: 'responseMethod', header: 'header_1', path, response: responseError },
      obj
    );
  });

  // #endregion

  // #region .returnsErrorResponseHeaderValue(call, obj?)

  it('.returnsErrorResponseHeaderValue({key, header, value, path, response}) test method returns error response header value', () => {
    test.returnsErrorResponseHeaderValue({
      key: 'responseMethod',
      header: 'header1',
      value: 'header_1',
      path,
      response: responseError
    });
  });

  it('.returnsErrorResponseHeaderValue({key, header, value, path, response}) test method returns error response header regexp value', () => {
    test.returnsErrorResponseHeaderValue({
      key: 'responseMethod',
      header: 'header1',
      value: '^h',
      path,
      response: responseError
    });
  });

  it('.returnsErrorResponseHeaderValue({key, header, value, path, response}, obj) test method returns error response header value with object', () => {
    test.returnsErrorResponseHeaderValue(
      {
        key: 'responseMethod',
        header: 'header1',
        value: 'header_1',
        path,
        response: responseError
      },
      obj
    );
  });

  // #endregion

  // #region .returnsNotErrorResponseHeaderValue(call, obj?)

  it('.returnsNotErrorResponseHeaderValue({key, header, value, path, response}) test method do not returns error response header value', () => {
    test.returnsNotErrorResponseHeaderValue({
      key: 'responseMethod',
      header: 'header1',
      value: 'header1',
      path,
      response: responseError
    });
  });

  it('.returnsNotErrorResponseHeaderValue({key, header, value, path, response}) test method do not returns error response header regexp value', () => {
    test.returnsNotErrorResponseHeaderValue({
      key: 'responseMethod',
      header: 'header1',
      value: '^a',
      path,
      response: responseError
    });
  });

  it('.returnsNotErrorResponseHeaderValue({key, header, value, path, response}, obj) test method do not returns error response header value with object', () => {
    test.returnsNotErrorResponseHeaderValue(
      {
        key: 'responseMethod',
        header: 'header1',
        value: 'header1',
        path,
        response: responseError
      },
      obj
    );
  });

  // #endregion

  // #region .mockErrorResponse(call)

  it('.mockErrorResponse({path, response}) mock error response without ErrorEvent', () => {
    test.returnsNotErrorResponseErrorInstance({
      key: 'responseMethod',
      instanceOf: ErrorEvent,
      path,
      response: responseError
    });
  });

  it('.mockErrorResponse({path, response}) mock error response with ErrorEvent', () => {
    test.returnsErrorResponseErrorInstance({
      key: 'responseMethod',
      instanceOf: ErrorEvent,
      path,
      response: responseErrorEvent
    });
  });

  // #endregion
});
