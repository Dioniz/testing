# How to test a service

Given an Angular service.

```typescript
@Injectable()
class TestService {}
```

First you must define the `ServiceTest` object. If the service have dependencies, we must create the `TestBedConfig` object and pass it as param to the ServiceTest. Otherwise, it can be omitted.

```typescript
describe('TestService', () => {
  let test: ServiceTest<TestService>;
  const config: TestBedConfig = new TestBedConfig();

  config.imports.push(FeatureModule);

  beforeEach(() => {
    test = new ServiceTest(TestService, config);
  });
});
```

Then we can use all [common class helpers](https://ngxa.gitlab.io/testing/classes/ClassTest.html), as well all [specific Angular helpers](https://ngxa.gitlab.io/testing/classes/AngularTestAbstract.html). The service object is under `test.obj`.
