# @ngxa/testing

Angular testing library.

[![npm](https://img.shields.io/npm/v/@ngxa/testing.svg)](https://www.npmjs.com/package/@ngxa/testing)
[![NpmLicense](https://img.shields.io/npm/l/@ngxa/testing.svg)](https://gitlab.com/ngxa/testing/blob/master/LICENSE.md)
[![coverage report](https://gitlab.com/ngxa/testing/badges/master/coverage.svg)](https://gitlab.com/ngxa/testing/commits/master)
[![sonarqube](https://sonarcloud.io/api/project_badges/measure?project=ngxa%3Atesting&metric=alert_status)](https://sonarcloud.io/dashboard?id=ngxa%3Atesting)
[![documentation](https://img.shields.io/badge/web-docs-orange.svg)](http://ngxa.gitlab.io/testing)
[![coverage](https://img.shields.io/badge/web-coverage-orange.svg)](http://ngxa.gitlab.io/testing/coverage/)
[![tests](https://img.shields.io/badge/web-tests-orange.svg)](http://ngxa.gitlab.io/testing/coverage/unit-tests-report.html)

## Installation

You can install @ngxa/testing, and all its dependencies, using npm.

```shell
npm install --save-dev @ngxa/testing
```

## Usage

This library provides a set of helpers to test Angular projects. A helper is a method that generates a test. You can also avoid all `TestBed` configration and use common test methods.

You can read the [full documentation](http://ngxa.gitlab.io/testing), as well get the [coverage](http://ngxa.gitlab.io/testing/coverage/) and [all the tests](http://ngxa.gitlab.io/testing/coverage/unit-tests-report.html) done to the library. In fact, the last one is the best documentation if you want to know exactly how to use each helper.

Actually the library offers:

- [ClassTest](https://ngxa.gitlab.io/testing/classes/ClassTest.html): a helper to test basic classes.
- [ModuleTest](https://ngxa.gitlab.io/testing/classes/ModuleTest.html): a helper to test Angular modules.
- [ServiceTest](https://ngxa.gitlab.io/testing/classes/ServiceTest.html): a helper to test Angular services.
- [HttpClientServiceTest](https://ngxa.gitlab.io/testing/classes/HttpClientServiceTest.html): a helper to test Angular HttpClient services.
- [ComponentTest](https://ngxa.gitlab.io/testing/classes/ComponentTest.html): a helper to test Angular components.
