## [0.6.3](https://gitlab.com/ngxa/testing/compare/v0.6.2...v0.6.3) (2018-11-06)


### Bug Fixes

* **http-client-service:** add test for returned values ([e95eb6b](https://gitlab.com/ngxa/testing/commit/e95eb6b))

## [0.6.2](https://gitlab.com/ngxa/testing/compare/v0.6.1...v0.6.2) (2018-11-06)


### Bug Fixes

* **package:** update package.json to fix versions ([d61ac7e](https://gitlab.com/ngxa/testing/commit/d61ac7e))

## [0.6.1](https://gitlab.com/ngxa/testing/compare/v0.6.0...v0.6.1) (2018-11-01)


### Bug Fixes

* **docs:** add documentation ([176c691](https://gitlab.com/ngxa/testing/commit/176c691))

# [0.6.0](https://gitlab.com/ngxa/testing/compare/v0.5.1...v0.6.0) (2018-10-30)


### Features

* **class:** add test for console output outputIsCalled ([e7de98f](https://gitlab.com/ngxa/testing/commit/e7de98f))

## [0.5.1](https://gitlab.com/ngxa/testing/compare/v0.5.0...v0.5.1) (2018-10-25)


### Bug Fixes

* **api:** add elements to api ([114b66b](https://gitlab.com/ngxa/testing/commit/114b66b))

# [0.5.0](https://gitlab.com/ngxa/testing/compare/v0.4.1...v0.5.0) (2018-10-25)


### Features

* **angular:** add router event test and docs ([2892075](https://gitlab.com/ngxa/testing/commit/2892075))
* **component:** add router to every Angular test ([65df053](https://gitlab.com/ngxa/testing/commit/65df053))

## [0.4.1](https://gitlab.com/ngxa/testing/compare/v0.4.0...v0.4.1) (2018-10-21)


### Bug Fixes

* **readme:** fix readme badges ([a0da4a2](https://gitlab.com/ngxa/testing/commit/a0da4a2))

# [0.4.0](https://gitlab.com/ngxa/testing/compare/v0.3.1...v0.4.0) (2018-10-21)


### Bug Fixes

* **component:** remove not used deps ([803a56a](https://gitlab.com/ngxa/testing/commit/803a56a))


### Features

* **angular-test:** add isInjected ([2563fb7](https://gitlab.com/ngxa/testing/commit/2563fb7))
* **component:** add hasElementClass ([f1b385f](https://gitlab.com/ngxa/testing/commit/f1b385f))
* **mocks:** add MockActivatedRoute ([8dcc50d](https://gitlab.com/ngxa/testing/commit/8dcc50d))

## [0.3.1](https://gitlab.com/ngxa/testing/compare/v0.3.0...v0.3.1) (2018-10-19)


### Bug Fixes

* **depsendencies:** updated dependencies and use of utils ([5fa47e8](https://gitlab.com/ngxa/testing/commit/5fa47e8))

# [0.3.0](https://gitlab.com/ngxa/testing/compare/v0.2.1...v0.3.0) (2018-10-17)


### Features

* **class:** add inheritsFrom test ([c5a912a](https://gitlab.com/ngxa/testing/commit/c5a912a))

## [0.2.1](https://gitlab.com/ngxa/testing/compare/v0.2.0...v0.2.1) (2018-10-16)


### Bug Fixes

* **package:** optional dependency ([2986f2f](https://gitlab.com/ngxa/testing/commit/2986f2f))

# [0.2.0](https://gitlab.com/ngxa/testing/compare/v0.1.1...v0.2.0) (2018-10-15)


### Features

* **angular:** add tests to check if an import, declaration or provider is or not present ([6ab6907](https://gitlab.com/ngxa/testing/commit/6ab6907))
* **module:** add a new ModuleTest to test NgModules ([f03fb8f](https://gitlab.com/ngxa/testing/commit/f03fb8f))

## [0.1.1](https://gitlab.com/ngxa/testing/compare/v0.1.0...v0.1.1) (2018-10-15)


### Bug Fixes

* **style:** reestyle and refactor code ([89a31df](https://gitlab.com/ngxa/testing/commit/89a31df))

# [0.1.0](https://gitlab.com/ngxa/testing/compare/v0.0.1...v0.1.0) (2018-10-13)


### Bug Fixes

* **angular:** add extra args to test constructor and setTestBed ([582c911](https://gitlab.com/ngxa/testing/commit/582c911))
* **class:** better class testing helpers ([435f945](https://gitlab.com/ngxa/testing/commit/435f945))
* **class:** better detection of properties, including getters/setters, and methods ([a28b4e2](https://gitlab.com/ngxa/testing/commit/a28b4e2))
* **class:** fix methodCalled and more tests ([f5a183a](https://gitlab.com/ngxa/testing/commit/f5a183a))
* **class:** make `.obj` readonly ([631a2df](https://gitlab.com/ngxa/testing/commit/631a2df)), closes [#1](https://gitlab.com/ngxa/testing/issues/1)
* **class:** remove null from types ([f80628e](https://gitlab.com/ngxa/testing/commit/f80628e))
* **component:** fix comparation ([011466a](https://gitlab.com/ngxa/testing/commit/011466a))
* **http-service:** fix types ([33fdfe0](https://gitlab.com/ngxa/testing/commit/33fdfe0))
* **routing:** fix RoutingComponentTest ([a1daad5](https://gitlab.com/ngxa/testing/commit/a1daad5))
* **test:** add parent test class ([d2a4978](https://gitlab.com/ngxa/testing/commit/d2a4978))
* **utils:** move functions to utils ([fd4bb08](https://gitlab.com/ngxa/testing/commit/fd4bb08))


### Features

* **angular:** add angular basic testings ([1f11624](https://gitlab.com/ngxa/testing/commit/1f11624))
* **api:** add elements to API ([ba1add5](https://gitlab.com/ngxa/testing/commit/ba1add5))
* **api:** basic structure for the api ([64b402e](https://gitlab.com/ngxa/testing/commit/64b402e))
* **class:** add methods to class test ([049fe47](https://gitlab.com/ngxa/testing/commit/049fe47))
* **class:** add more functionality to class test ([c5dec5b](https://gitlab.com/ngxa/testing/commit/c5dec5b))
* **class:** add more options to class tests ([4f5f03c](https://gitlab.com/ngxa/testing/commit/4f5f03c))
* **class:** add new test methods ([c5290dc](https://gitlab.com/ngxa/testing/commit/c5290dc))
* **class:** add Observable and Promise error specific tests ([54210f8](https://gitlab.com/ngxa/testing/commit/54210f8))
* **class:** add option to test async errors ([0f785eb](https://gitlab.com/ngxa/testing/commit/0f785eb))
* **class:** first implementation of abstract class test helper ([3b1c475](https://gitlab.com/ngxa/testing/commit/3b1c475))
* **class:** first implementation of class test helper ([da0e98c](https://gitlab.com/ngxa/testing/commit/da0e98c))
* **class:** refactor methods and add getter and setter tests ([270412a](https://gitlab.com/ngxa/testing/commit/270412a))
* **class:** remove FromKey ([e0eaf0e](https://gitlab.com/ngxa/testing/commit/e0eaf0e))
* **class:** sepparate prop and method velue tests and code refactor ([ce56870](https://gitlab.com/ngxa/testing/commit/ce56870))
* **class:** test secondary class method call ([7a9d1c1](https://gitlab.com/ngxa/testing/commit/7a9d1c1))
* **component:** add Event test ([6561e6f](https://gitlab.com/ngxa/testing/commit/6561e6f))
* **component:** add more methods to test el values and attr ([60b9d46](https://gitlab.com/ngxa/testing/commit/60b9d46))
* **component:** add test element property ([7e17214](https://gitlab.com/ngxa/testing/commit/7e17214))
* **component:** first TestComponent methods ([da6c4a9](https://gitlab.com/ngxa/testing/commit/da6c4a9))
* **component:** new and rewrited component tests ([e7ff7eb](https://gitlab.com/ngxa/testing/commit/e7ff7eb))
* **http:** HttpServiceTest get value and error methods ([04273f4](https://gitlab.com/ngxa/testing/commit/04273f4))
* **http-client:** match regexp from headers and params ([e08726a](https://gitlab.com/ngxa/testing/commit/e08726a))
* **http-service:** add methods to test HtppRequest ([f070443](https://gitlab.com/ngxa/testing/commit/f070443))
* **http-service:** add more methods to test Http calls ([23e72d5](https://gitlab.com/ngxa/testing/commit/23e72d5))
* **http-service:** add new methods and tests ([674cd44](https://gitlab.com/ngxa/testing/commit/674cd44))
* **http-service:** add tests to typeOf and instanceOf and refactor ([212c5d2](https://gitlab.com/ngxa/testing/commit/212c5d2))
* **router:** add basic setup to test routing ([8b5f212](https://gitlab.com/ngxa/testing/commit/8b5f212))
* **service:** test services ([8126c54](https://gitlab.com/ngxa/testing/commit/8126c54))
* **typings:** add typings ([800c793](https://gitlab.com/ngxa/testing/commit/800c793))
* **util:** define getPropertyDescriptor with heritage ([3eda328](https://gitlab.com/ngxa/testing/commit/3eda328))
* **utils:** add isHttpEvent ([0653925](https://gitlab.com/ngxa/testing/commit/0653925))
* **utils:** refactor and add utils ([012d0dc](https://gitlab.com/ngxa/testing/commit/012d0dc))
